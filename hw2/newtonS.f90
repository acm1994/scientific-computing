!
! A simple example: solving an equation f(x) = 0
! using Newton's method
!
! This is the template file used for the scripted version  
!
program newton
  
  implicit none
  double precision :: f, fp, x, dx, abs_err, x_prev, abs_err_prev, lin, quad
  integer :: iter
  
  ! Here we try to find the solution to f(x) = 0

  ! First, we are going to initialize the current absolute value of the
  ! error as 1 and initialize the previous absolute value of the error as 1
  ! as well.
  abs_err = 1
  abs_err_prev = 1

  ! Begin iterations at 0.
  iter = 0

  ! Intialize starting position for the algorithm at x = -0.5.
  x = -0.5d0
  
  do while(abs_err > 10d-15)

     ! Update the iteration count.
     iter = iter + 1

     ! Here, we are going to compute the value of the function at the point
     ! in which we set as the starting point (i.e., x = -0.5d0).
     f = ffun(x)

     ! Since Newton's Method requires the derivative of the function at a particular
     ! point, this line of code will be responsible for calculating the value of the
     ! derivative of the function at the current value of 'x'.
     fp = fpfun(x)

     ! In order to find the next ponit of 'x' to be used for the next Newton
     ! iteration, we are going to implement the tangent line of the function
     ! currently being used by finding the point at which it contacts the
     ! x-axis. The difference between this new point and the current point on
     ! the x-axis wll be denoated as dx.
     dx = -f/fp

     ! Assign the current value of 'x' as the variable which will denote the
     ! previous value of 'x'.
     x_prev = x

     ! Update the value of 'x' with the next estimate resulting from Newton's
     ! Method.
     x = x + dx
	       
     ! Assign the current absolute value of the error to the
     ! variable which holds the previous absolute value of the
     ! error.
     abs_err_prev = abs_err

     ! Calculate absolute error.
     abs_err = abs(x - x_prev)

     ! In order to calculate the rate of convergence, we are going to
     ! provide the ratios which correspond to linear and quadratic convergence.
     lin = abs_err / abs_err_prev
     quad = abs_err / (abs_err_prev**2)

     ! Write out values to the screen.
     write(*,'(A18,I2.2,2(E24.16), 2(E24.16))') ' sin(x)+cos(x*x) ', iter, x, dx, lin, quad
     
  end do

contains

  double precision function ffun(x)
    implicit none
    double precision :: x

    ffun = sin(x)+cos(x*x)

  end function ffun

  double precision function fpfun(x)
    implicit none
    double precision :: x

    fpfun = cos(x)-2.d0*x*sin(x*x)

  end function fpfun

end program newton
