%----------------------------------------------------------------------------------------
%	PACKAGES AND OTHER DOCUMENT CONFIGURATIONS
%----------------------------------------------------------------------------------------

\documentclass[paper=a4, fontsize=11pt]{scrartcl} % A4 paper and 11pt font size

\usepackage[T1]{fontenc} % Use 8-bit encoding that has 256 glyphs
\usepackage{fourier} % Use the Adobe Utopia font for the document - comment this line to return to the LaTeX default
\usepackage[english]{babel} % English language/hyphenation
\usepackage{amsmath,amsfonts,amsthm} % Math packages

\usepackage{lipsum} % Used for inserting dummy 'Lorem ipsum' text into the template
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{graphicx}
\graphicspath{C:/Users/stema/OneDrive/Documents/MATH 471}

\usepackage{sectsty} % Allows customizing section commands
\allsectionsfont{\centering \normalfont\scshape} % Make all sections centered, the default font and small caps

\usepackage{fancyhdr} % Custom headers and footers
\pagestyle{fancyplain} % Makes all pages in the document conform to the custom headers and footers
\fancyhead{} % No page header - if you want one, create it in the same way as the footers below
\fancyfoot[L]{} % Empty left footer
\fancyfoot[C]{} % Empty center footer
\fancyfoot[R]{\thepage} % Page numbering for right footer
\renewcommand{\headrulewidth}{0pt} % Remove header underlines
\renewcommand{\footrulewidth}{0pt} % Remove footer underlines
\setlength{\headheight}{13.6pt} % Customize the height of the header

\numberwithin{equation}{section} % Number equations within sections (i.e. 1.1, 1.2, 2.1, 2.2 instead of 1, 2, 3, 4)
\numberwithin{figure}{section} % Number figures within sections (i.e. 1.1, 1.2, 2.1, 2.2 instead of 1, 2, 3, 4)
\numberwithin{table}{section} % Number tables within sections (i.e. 1.1, 1.2, 2.1, 2.2 instead of 1, 2, 3, 4)

\setlength\parindent{0pt} % Removes all indentation from paragraphs - comment this line for an assignment with lots of text

%----------------------------------------------------------------------------------------
%	TITLE SECTION
%----------------------------------------------------------------------------------------

\newcommand{\horrule}[1]{\rule{\linewidth}{#1}} % Create horizontal rule command with 1 argument of height

\title{	
\normalfont \normalsize 
\textsc{University of New Mexico, Department of Mathematics and Statistics} \\ [25pt] % Your university, school and/or department name(s)
\horrule{0.5pt} \\[0.4cm] % Thin top horizontal rule
\huge MATH 471: Newton's Method For Finding Roots \\ % The assignment title
\horrule{2pt} \\[0.5cm] % Thick bottom horizontal rule
}

\author{Joseph Murrietta and Aaron Montano} % Your name

\date{\normalsize\today} % Today's date or a custom date

\begin{document}

\maketitle % Print the title

\section*{Introduction and Motivation}

\begin{flushleft}
Let $f : \Re \rightarrow \Re$. Suppose you wished to find a root of $f$ (i.e., $x \in \Re : f(x) = 0$). In many cases, the implementation of simple "by-hand" algebraic routines would yield an exact answer. But what if we are interested in finding the root via computation? Unfortunately, the task is not as simple as setting $f = 0$ and moving terms around to find $x$ for that particular case. When carrying out root-finding on a computer, the seemingly trivial task must be addressed with a different approach. While there already exist many one-line methods within programming languages that will find the root(s) of a function within a fraction of a second  (such as methods from the \texttt{scipy.optimize} package in \textit{Python}), we are interested in exploring what is going on beneath the surface. For this write-up, we will be taking a look at an iterative root-finding method known as \textbf{Newton's Method} by implementing it on three different functions. Particularly, we will be examining its algorithm and efficiency.
\end{flushleft}

\section*{Newton's Method: Overview}

\begin{flushleft}
Recall from our introduction that Newton's Method is an \textit{iterative} root-finding method, meaning that a particular computation is ran a number of times. While it is possible for only one iteration of the algorithm to run (i.e., guessed correctly on the first time as we will discuss later), this is usually not the case. Rather, the algorithm is repeated until one or more of three different criteria is met: 
\end{flushleft}

\begin{itemize}
\item A root of the function is successfully found (i.e., we find the value $x$ such that $f(x) = 0$).
\item We enter an acceptable, pre-determined error bound.
\item We reach our limit when it comes to the number of iterations carried out.
\end{itemize}

\begin{flushleft}
So how is Newton's Method actually carried out? Let $f(x)$ be a continuous, differentiable function over a selected interval $[\alpha, \beta] : \alpha , \beta \in \Re$. Our first step is to take an initial guess as to where the root(s) is (it is important to note that it is extremely uncommon that the root is correctly guessed right away). At this particular point, $x_0$, we go up to the point $f(x_0)$ on the function. From there, we take a look at the tangent line at this location. The tangent line of this point of $f(x)$ will intersect the x-axis at $x_{1}$. Up to now, we have carried out a single iteration of the method. These iterations will be repeated in the sense that the new value $x_{i+1}$ in which the tangent line of $f(x_{i})$ intersects the x-axis is used as the new "initial guess" in the subsequent iteration. Mathematically, Newton's Method can be described as such:
\end{flushleft}

\begin{center}
$
x_{i+1} = x_{i} - \frac{f(x_{i})}{f'(x_{i})} \hspace{1mm}
: \hspace{1mm}
i = 0, 1, 2, \ldots , n
$
\end{center}

\begin{flushleft}
$x_{0}$ is our \textbf{initial guess} of the root and is the value that will kick-start the algorithm.
\end{flushleft}

\section*{Application}

\begin{flushleft}
Now that we have a general understanding of the algorithm behind Newton's Method, it is now time to put it into practice. In this section of the write-up, we are going to implement Newton's Method on three different, differentiable, real-valued functions. In order to see what is going on with each iteration, a table of selected values from the algorithm will be provided for each function. The functions in which we are going to perform Newton's Method on are $f(x) = x$, $f(x) = x^{2}$, and $f(x) = sin(x) + cos(x^{2})$. 
\end{flushleft}

\begin{center}
\begin{LARGE}
$f(x) = x$
\end{LARGE}
\end{center}

\begin{center}
\includegraphics[scale=.7]{x}
\end{center}

\begin{flushleft}
As an initial guess, we will start out with $x_{0} = -0.5$. In the table below, the first column represents the current iteration, the second column is the value of $x$ for the respective iteration and the third column displays $\Delta x$, which is the difference between $x_{i}$ and $x_{i+1}$.
\end{flushleft}

\begin{center}
\begin{tabular}{|p{0.3cm}||p{4cm}|p{4.5cm}|}
 \hline
 \multicolumn{3}{|c|}{Iterative Positions of $f(x) = x$} \\
 \hline
 $i$ & $x$ & $\Delta x$ \\
 \hline
 \input{table1.tex}
 \hline
\end{tabular}
\end{center}

\begin{flushleft}
In the table below, the first column represents the current iteration just as was done in the previous table. The second column displays the value for linear convergence ($\vert E \vert_{i+1} \approx C * \vert E_{i} \vert : C \in \Re$) and quadratic convergence ($\vert E_{i+1} \vert \approx C * \vert E_{i}^{2} \vert : C \in \Re$)
\end{flushleft}

\begin{center}
\begin{tabular}{|p{0.3cm}||p{4cm}|p{4.5cm}|}
 \hline
 \multicolumn{3}{|c|}{Linear and Quadratic Convergence For $f(x) = x$} \\
 \hline
 $i$ & $\vert E_{i+1} \vert / \vert E_{i} \vert$ & $\vert E_{i+1} \vert / \vert E_{i}^{2} \vert$ \\
 \hline
 \input{table1b.tex}
 \hline
\end{tabular}
\end{center}

\begin{center}
\begin{LARGE}
	$f(x) = x^{2}$
\end{LARGE}
\end{center}

\begin{center}
\includegraphics[scale=.7]{x2}
\end{center}

\begin{flushleft}
As we did for $f(x) = x$, our initial guess will begin with $x_{0} = -0.5$.
\end{flushleft}

\begin{center}
\begin{tabular}{|p{0.3cm}||p{4.4cm}|p{4.5cm}|}
 \hline
 \multicolumn{3}{|c|}{Iterative Positions of $f(x) = x^{2}$} \\
 \hline
 $i$ & $x$ & $\Delta x$ \\
 \hline
 \input{table2a.tex}
 \hline
\end{tabular}
\end{center}

\begin{center}
\begin{tabular}{|p{0.3cm}||p{4.4cm}|p{4.5cm}|}
 \hline
 \multicolumn{3}{|c|}{Iterative Positions of $f(x) = x^{2}$ (cont.)} \\
 \hline
 $i$ & $x$ & $\Delta x$ \\
 \hline
 \input{table2aa.tex}
 \hline
\end{tabular}
\end{center}

\begin{center}
\begin{tabular}{|p{0.3cm}||p{4cm}|p{4.5cm}|}
 \hline
 \multicolumn{3}{|c|}{Linear and Quadratic Convergence For $f(x) = x^{2}$} \\
 \hline
 $i$ & $\vert E_{i+1} \vert / \vert E_{i} \vert$ & $\vert E_{i+1} \vert / \vert E_{i}^{2} \vert$ \\
 \hline
 \input{table2b.tex}
 \hline
\end{tabular}
\end{center}

\begin{center}
\begin{tabular}{|p{0.3cm}||p{4cm}|p{4.5cm}|}
 \hline
 \multicolumn{3}{|c|}{Linear and Quadratic Convergence For $f(x) = x^{2}$ (cont.)} \\
 \hline
 $i$ & $\vert E_{i+1} \vert / \vert E_{i} \vert$ & $\vert E_{i+1} \vert / \vert E_{i}^{2} \vert$ \\
 \hline
 \input{table2bb.tex}
 \hline
\end{tabular}
\end{center}

\begin{center}
\begin{LARGE}
$f(x) = sin(x) + cos(x^{2})$
\end{LARGE}
\end{center}

\begin{center}
\includegraphics[scale=.7]{trig}
\end{center}

\begin{flushleft}
As we did for the previous two functions, we will set our initial position $x_{0} = -0.5$. 
\end{flushleft}

\begin{center}
\begin{tabular}{|p{0.3cm}||p{4.4cm}|p{4.5cm}|}
 \hline
 \multicolumn{3}{|c|}{Iterative Positions of $f(x) = sin(x) + cos(x^{2})$} \\
 \hline
 $i$ & $x$ & $\Delta x$ \\
 \hline
 \input{table3a.tex}
 \hline
\end{tabular}
\end{center}

\begin{center}
\begin{tabular}{|p{0.3cm}||p{4.3cm}|p{4.5cm}|}
 \hline
 \multicolumn{3}{|c|}{Linear and Quadratic Convergence For $f(x) = sin(x) + cos(x^{2})$} \\
 \hline
 $i$ & $\vert E_{i+1} \vert / \vert E_{i} \vert$ & $\vert E_{i+1} \vert / \vert E_{i}^{2} \vert$ \\
 \hline
 \input{table3b.tex}
 \hline
\end{tabular}
\end{center}

\section*{Convergence Analysis}

\begin{flushleft}
One of the most important questions that is asked regarding algorithms of any type is how it scores with efficiency. That is, how long does it take to achieve the task it was written for. While for a not-so-complex problem such as finding the root(s) of a function it does not seem to make a difference, if you are ever in need of performing an enormous computation with hundreds of thousands of iterations, writing efficient code can literally determine whether the script will take one hour or an entire week to run. For the sake of simplicity, we are going to give qualitative efficiency observations for the examples we used earlier to demonstrate Newton's Method for finding roots (i.e., we will simply state whether the algorithm "linearly converged", "quadratically converged" or "did not converge" to describe what occurred at run time rather than discuss qualitative variables). 
\end{flushleft}

\begin{flushleft}
Recall from numerical computing that if a root has a multiplicity greater than 1 (i.e., $m > 1$), then the rate of convergence is linear. In other words, the computation error is reduced by a constant factor for each iteration). Multiplicity in terms of roots for polynomials can be thought of the number of times the function has a root at a specific point. If we also recall from numerical computing, graphically speaking, $f'(x)$ is \textit{not} tangent to the x-axis at a simple root (i.e., $m = 1$) and \textit{is} tangent to the x-axis at points of multiple roots (particularly when $m$ is even at these points). Informally speaking, the plot of a polynomial $f(x)$ goes straight through the x-axis at roots of odd multiplicity and merely touches (but does not cross) the x-axis at roots of even multiplicity. However, it is also important to note that if a function "wiggles" around its root as it does with $f(x) = x^{3}$ or $f(x) = x^{5}$, then the multiplicity of the root \textit{is an odd number not equal to 1}. If a function just goes straight through, then the multiplicity at that root is 1.
\end{flushleft}

\begin{flushleft}
From our information in the previous paragraph, we conclude that $f(x) = x$ \textit{quadratically} converges due to the fact that it contains a simple root at $x = 0$. If we take a look at the plot of $f(x) = x^{2}$, we will notice that at $x = 0$ the function bounces off the x-axis but does not go through. As a result, we can say that the function has even multiplicity greater than 0 at $x = 0$ (this case cannot have a multiplicity of 0, otherwise, there would not be a root). Therefore, we conclude that $f(x)$ \textit{linearly} converges to its root. As for $f(x) = sin(x) + cos(x^{2})$, we can see on its plot that goes straight through the x-axis near our initial point ($x_0 = -0.5$). Since this polynomial does not "wiggle" at this point, we can conclude that $f(x) = sin(x) + cos(x^{2})$ contains a simple root near $x_0 = -0.5$ and therefore \textit{quadratically} converges.
\end{flushleft}

\begin{flushleft}
Even though we are completely confident that there exist algorithms which find the roots of functions at a much more rapid rate than Newton's Method, we are interested in finding a way to increase the efficiency of it for roots that linearly converge. Fortunately, there already exists such a fix. For our examples, only one of our functions, $f(x) = x^{2}$, converged at a slower (i.e., linear) rate. How can we alter Newton's Method to achieve quadratic convergence? Fortunately for us, if we know the multiplicity $m$ for a polynomial root, the Newton's Method algorithm just needs to be altered like so to achieve quadratic convergence.
\end{flushleft}

\begin{center}
$
x_{i+1} = x_{i} - m \frac{f(x_{i})}{f'(x_{i})}
$
\end{center}

\begin{flushleft}
For our particular case of $f(x) = x^{2}$, we know that $m = 2$. Therefore
\end{flushleft}

\begin{center}
$
x_{i+1} = x_{i} - 2 \frac{f(x_{i})}{f'(x_{i})}
$
\end{center}

\begin{flushleft}
is a modified version of Newton's Method that retains quadratic convergence.
\end{flushleft}

\end{document}