#!/usr/apps/bin/perl
#
# A simple program to format text of outfrom from newtonS.pl
#
#
#

$cmdFile ="./tmp.txt";
$outFile ="./updatedtmp.txt";

  open(FILE, "$cmdFile") || die "cannot open";
  open(OUTFILE, "> $outFile") || die "cannot open";

  while( $line = <FILE> )
  {
	  $line =~ s/\s+/ , /g;
	  $line = $line . "\n";
	  $line =~ s/ , $/ /;
	  $line =~ s/ , / /;
	  print OUTFILE $line;
  }

  close (OUTFILE);
  close (FILE);
  system("rm tmp.txt");
  exit
