***********************************************************
README File for HW2, Math 471, J. Murrietta & A. Montano
***********************************************************

- This homework is used to show Newton's method can be used
  to find the roots of functions, f(x) = 0. 
  And to analyze our findings.
- newtonS.pl is a script file that allows the newtonS.f90.Template
  to be ran on muliple functions at a time by creating the newtonS.f90
  for each funtion and outputing results.
- newtonS.f90.Template is a fortran file that is read over by the script
  file to create the newtonS.f90 file. This has the overall structure of
  newtons's method.
- updatedtmp.txt is the formatted output.


- To run this to reproduce results, do the following in the console:
  - perl newtonS.pl (performs newtons method).
  - perl edit.pl (formats output to updatedtmp.txt).
  - vim updatedtmp.txt to view results.

- Results can also be viewed in the report H2report.pdf.

