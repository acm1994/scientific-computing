# Makefile for Gaussian Quadrature.

gauss.x: lglnodes.o gaussQuad.o
	gfortran lglnodes.o gaussQuad.o -o gauss.x

gaussQuad.o: gaussQuad.f90
	gfortran -c gaussQuad.f90

lglnodes.o: lglnodes.f90
	gfortran -c lglnodes.f90