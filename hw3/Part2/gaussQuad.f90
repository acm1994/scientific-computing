  ! Joseph Murrietta and Aaron Montano
  ! 09 / 30 / 2017
  ! Homework 3, Part 2: Integration Via Gaussian Quadrature.

  ! This subroutine will be resopnsible for computing the integral of a continuous
  ! function f(x) via Gaussian Quadrature.
subroutine gaussQkpi

  ! Set implicit to none.
  implicit none

  ! Declaration of double precision variables (allocatable).
  ! x = grid points.
  ! w = weights for each respective grid point.
  ! f = function value
  double precision, allocatable :: x(:), w(:), f(:)

  ! Declaration of integral value, previous value, error and pi.
  double precision :: int_val, pi, prev_val, error

  ! Out unit declaration.
  integer, parameter :: out_unit = 100
  
  ! Declaration of integer variables.
  ! n = number of grid points.
  ! j, i = loop iterators.
  integer :: n, j, i

  ! Create value for pi.
  pi = (4 * atan(1.0))

  ! Initialize n and previous value.
  prev_val = 2
  n = 1200

  ! Print out header.
  print *, "k = pi"

  ! Open file to write to it.
  open (unit = out_unit, file = "Gresults_pi.csv", action = "write", status="replace")
  write(out_unit, *) "n,err,"
  
  ! do loop.
  do i = 2, n

     ! Allocate all of the arrays.
     allocate(x(0:i), f(0:i), w(0:i))

     ! Call lglnodes subroutine.
     call lglnodes(x, w, i)

     ! Calculate the function at all gridpoints.
     f = exp(cos(pi*x))

     ! Compute integral and error.
     int_val = sum(f*w)
     error = abs(int_val - prev_val)
     
     ! Deallocate all arrays.
     deallocate(x, f, w)

     ! Print out important values.
     print *, "n =", i, "int_val=", int_val, "error =", error

     ! Update previous value.
     prev_val = int_val

     write (out_unit, *) i,",",error,","
     
  end do

  ! Close file.
  close (out_unit)

end subroutine gaussQkpi

! This subroutine will be responsible for implementing Gaussian Quadrature on
! f = exp(cos(pi*pi*x)).
subroutine gaussQkpi2

  ! Set implicit to none.
  implicit none

  ! Declaration of double precision variables (allocatable).
  ! x = grid points.
  ! w = weights for each respective grid point.
  ! f = function value
  double precision, allocatable :: x(:), w(:), f(:)

  ! Declaration of integral value, previous value, error and pi.
  double precision :: int_val, pi, prev_val, error

  ! Out unit declaration.
  integer, parameter :: out_unit = 100
  
  ! Declaration of integer variables.
  ! n = number of grid points.
  ! j, i = loop iterators.
  integer :: n, j, i

  ! Create value for pi.
  pi = (4 * atan(1.0))

  ! Initialize n and previous value.
  prev_val = 2
  n = 1200

  ! Print out header.
  print *, "k = pi**2"

  ! Open file to write to it.
  open (unit = out_unit, file = "Gresults_pi2.csv", action = "write", status="replace")
  write(out_unit, *) "n,err,"
  
  ! do loop.
  do i = 2, n

     ! Allocate all of the arrays.
     allocate(x(0:i), f(0:i), w(0:i))

     ! Call lglnodes subroutine.
     call lglnodes(x, w, i)

     ! Calculate the function at all gridpoints.
     f = exp(cos(pi*pi*x))

     ! Compute integral and error.
     int_val = sum(f*w)
     error = abs(int_val - prev_val)

     ! Deallocate all arrays.
     deallocate(x, f, w)

     ! Print out important values.
     print *, "n =", i,  "int_val=", int_val, "error =", error

     ! Update previous value.
     prev_val = int_val

     ! Write the iteration number and error to file.
     write (out_unit, *) i,",",error,","

  end do

  ! Close file.
  close (out_unit)

end subroutine gaussQkpi2

program gaussQuad

  call gaussQkpi
  call gaussQkpi2
  
end program gaussQuad

