! Joseph Murrietta and Aaron Montano
! Homework 3 (part 1): Integration Via Trapezoidal Rule Over 'N' Subintervals.
! 09 / 27 / 2017

! This subroutine will perform the Trapezoidal Rule for n = 300 and k = pi.
subroutine kPi 

	implicit none
 
	!---------------------------------------------------------------------------------!
	
	! Declaration of loop iterators.
	integer :: i, j
	
	! Total number of iterations.
	integer :: n 
	
	! Declaration of real variables. !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	! xL = lower bound of integral.
	! xR = upper bound of integral.
	! h = spacing.
	! k = pi
	! sum_curr = current sum.
	! abs_err = absolute error.
	! prev_result = previous result.
	! xi = current x position.
	! mean = mean value.
	! curr_result = current result.
        double precision :: k, abs_err, prev_result, curr_result 
        double precision, ALLOCATABLE :: xi(:), f(:), w(:)

        ! Out unit declaration.
        integer, parameter :: out_unit = 100
        
        ! Declaration of function variables.
         double precision :: x


	!-----------------------------------------------------------------------------------!
	
	! Declaration of variables which will be used throughout the entire subroutine.
        k = 4 * atan(1.0)
	n = 1200
	
	! Initialize previous result.
         prev_result = 2
	
	!-----------------------------------------------------------------------------------!

         ! Open file.
         open (unit = out_unit, file = "resultsGQ_pi.csv", action = "write", status="replace")
         write(out_unit, *) "n,err,"
	! Begin Guass Quadrature Algorithm (k = pi)
	
		! Run the Trapezoidal Rule for j equally spaced grid ponits.
		do i = 2, n
                allocate(xi(0:i), f(0:i), w(0:i))
                call lglnodes(xi, w, i)
                f =exp(cos(k*xi))
		
			! Sum up values.
                        
			curr_result = sum(f*w)
                        abs_err = (curr_result - prev_result)
                        deallocate(xi,f,w)
                        prev_result = curr_result

                        print *, "n =", i, "int_val=", curr_result, "error =", abs_err
			
		end do


             ! Close file.
             close (out_unit)
	
! Main function (y = exp(cos(k*x))).
contains

	double precision function ffun(x, k)
	
		implicit none
		double precision :: x, k
		
		ffun = exp(cos(k*x))
		
	end function ffun
 
end subroutine kPi

subroutine kPi2

	implicit none
	
	!---------------------------------------------------------------------------------!
	
	! Declaration of loop iterators.
	integer :: i, j
	
	! Total number of iterations.
	integer :: n 
	
	! Declaration of real variables. !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	! xL = lower bound of integral.
	! xR = upper bound of integral.
	! h = spacing.
	! k = pi
	! sum_curr = current sum.
	! abs_err = absolute error.
	! prev_result = previous result.
	! xi = current x position.
	! mean = mean value.
	! curr_result = current result.
	double precision :: xL, xR, h, k, sum_curr, abs_err, prev_result, xi, curr_result, summ, mean

        ! Declaration of function variables.
        double precision :: x

        ! Declaration of file.
        integer, parameter :: out_unit = 100
        
	!-----------------------------------------------------------------------------------!
	
	! Declaration of variables which will be used throughout the entire subroutine.
	xL = -1
        xR = 1
        k = (4 * atan(1.0)) ** 2
	n = 1200
	
	! Initialize previous result.
	prev_result = 1
	
	!-----------------------------------------------------------------------------------!

 ! Open file.
 open(unit = out_unit, file = "resultsGQ_pi2.csv", action = "write", status = "replace")
 ! Write variable headers.
 write(out_unit, *) "n,err,"
 
	! Begin Trapezoidal Rule Algorithm (k = pi)
	do j = 2, n 
	
		! Since we are at the beginning for an arbitrary value n, we will reset the values.
		summ = 0
		abs_err = 1
		xi = xL
		
		! Set up spacing.
		h = (xR - xL) / j
		
		! Run the Trapezoidal Rule for j equally spaced grid ponits.
		do i = 1, (j-1)
		
			! Update current x position.
			xi = xL + i*h
			
			! Sum up values.
			summ = summ + ffun(xi, k)
			
		end do

		! Retrieve and print out n, curr_result and abs_err.
		curr_result = h * (mean + summ)
		abs_err = abs(curr_result-prev_result)
		prev_result = curr_result
		print *, "n = ", j, "I = ", curr_result, "absolute error = ", abs_err

                write(out_unit, *) j,",",abs_err,","
  
	end do
	
! Main function (y = exp(cos(k*x))).
contains

	double precision function ffun(x, k)
	
		implicit none
		double precision :: x, k
		
		ffun = exp(cos(k*x))
		
	end function ffun

end subroutine kPi2

! Main program.
program main

	!print *, "K = PI"
        call kPi

        !print *, "K = PI^2"
        call kPi2

end program main

