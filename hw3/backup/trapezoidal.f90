  ! Joseph Murrietta, Aaron Montano.
  ! Homework 3: Integration Via Trapezoidal Rule Over N Subintervals.
  ! 09 / 21 / 2017
program trapezoidal
  
  implicit none

  ! Declaration of variables. (doubles)
  ! i = iterator
  ! n = number of segments.
  ! k = pi
  ! h = length of segments.
  ! fxL = function at xL.
  ! fxR = function at xR.
  ! err = error.
  ! sum = sum at current iteration.
  ! sum_prev = sum from previous iteration.
  ! mean = (f(x0) + f(xN)) / 2
  ! abs_err_prev = absolute value of the error from the previous iteration.
  ! abs_err = absolute error of current iteration.
  ! result = approx of the integral.
  ! xL = lower bound.
  ! xR = upper bound.
  double precision :: h, sum, mean, abs_err, result, result_prev, xL, xR, i, n, xi, result_known, pi

  ! Declaration of xL, xR, n and mean.
  xL = -1
  xR = 1
  mean = (ffun(xL) + ffun(xR)) / 2 
  pi = 4*atan(1.0)
  n = 1

  abs_err = 1

  do while (abs_err > 10d-10)

     ! Increment iterator.
     sum = 0
     abs_err = 1
     n = n * 2
     xi = xL
     i = 1

     h = (xR - xL) / n
     
     do while (i <= n-1 ) 
       ! Update x-value.
       xi = xi + h
       i = i + 1

       ! Sum up cummulative values.
       sum = sum + ffun(xi)

     end do
       result_prev = result
       result = h*(mean + sum)
       abs_err = abs(result - result_prev)
     
     write(*, '(4(f24.16))') h, n, abs_err, result
     !write(*, '((f24.16))') abs_err

  end do

  ! Retrieve final result and print it out.
  write(*, '(F24.16)') result
  
  ! Declaration of functions.
contains

  ! Main function: f(x) = exp(cos(3.14*x)).
  double precision function ffun(x)
    
    implicit none
    double precision :: x

    ffun = exp(cos(pi*x))

  end function ffun
  
end program trapezoidal
