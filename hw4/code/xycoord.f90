module xycoord
  real(kind = 8), parameter :: pi = acos(-1.d0)
  save
  contains
  
  real(kind=8) function x_coord(r,s)
    implicit none
    real(kind=8) r,s, PI
    pi = 4.d0*atan(1.d0)
    x_coord = (6.5d0+r+5.d0*s)*cos(4.d0*s) ! Third plot.
    !x_coord = (2.d0+r+0.2*sin(5.d0*pi*s))*cos(0.5d0*pi*s) ! Example.
    !x_coord = r**3 ! Second plot.
    !x_coord = r + s ! Verification.
  end function x_coord

  real(kind=8) function y_coord(r,s)
    implicit none
    real(kind=8) r,s, PI
    PI = 4.d0*atan(1.d0)
    y_coord = (6.5d0+r+5.d0*s)*sin(4.d0*s) ! Third plot.
    !y_coord = (2.d0+r+0.2*sin(5.d0*pi*s))*sin(0.5d0*pi*s) ! Example.
    !y_coord = s**2 + r ! Second plot.
    !y_coord = r - s
  end function y_coord
    
end module xycoord
