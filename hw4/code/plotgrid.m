clear all
close all
load x.txt 
load y.txt 


%plot(x,y,'k',x',y','k')
%  xlabel('X')
%  ylabel('Y')
%  title('Initial Script')
%grid on
%axis equal
%print -depsc2 grid.bmp

% Change to appropriate .csv format.
[i, j] = size(x);
X = zeros(i+1, j);
X(2:(i+1), 1:j) = x;
X(1, 1:j) = [1:j];
[i, j] = size(y);
Y = zeros(i+1, j);
Y(2:(i+1), 1:j) = y;
Y(1, 1:j) = [1:j];
  
% Write to .csv file.
  csvwrite('x.csv', X);
csvwrite('y.csv', Y);

xT = x';
yT = y';

% Change to appropriate .csv format.
[i, j] = size(xT);
XT = zeros(i+1, j);
XT(2:(i+1), 1:j) = xT;
XT(1, 1:j) = [1:j];
[i, j] = size(yT);
YT = zeros(i+1, j);
YT(2:(i+1), 1:j) = yT;
YT(1, 1:j) = [1:j];
csvwrite('xT.csv', XT);
csvwrite('yT.csv', YT);

load ux.txt
load uy.txt

% Change to appropriate .csv format.
[i, j] = size(x);
X = zeros(i+1, j);
X(2:(i+1), 1:j) = x;
X(1, 1:j) = [1:j];
[i, j] = size(y);
Y = zeros(i+1, j);
Y(2:(i+1), 1:j) = y;
Y(1, 1:j) = [1:j];

% Write to .csv file.
csvwrite('ux.csv', X);
csvwrite('uy.csv', Y);

load err1.txt

% Change to appropriate .csv format.
[i, j] = size(x);
X = zeros(i+1, j);
X(2:(i+1), 1:j) = x;
X(1, 1:j) = [1:j];
[i, j] = size(y);
Y = zeros(i+1, j);
Y(2:(i+1), 1:j) = y;
Y(1, 1:j) = [1:j];

csvwrite('err1.csv', X)
