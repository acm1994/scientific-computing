program hwk4
  use xycoord   !use the module xycoord to set the mapping 
  implicit none
  integer :: nr,ns,i,j
  integer :: k ! For loop iterator for error calculation section.
  real(kind = 8) :: hr,hs
  real(kind = 8) :: t ! Update value.
  real(kind = 8), dimension(:), allocatable :: r,s
  real(kind = 8), dimension(:,:), allocatable :: u,ur,us
  real(kind = 8), dimension(:, :), allocatable :: err1 ! Error approximation.

  ! Allocate memory for the arrays which will hold partial derivatives.
  real(kind = 8), dimension(:, :), allocatable :: xr, xs, yr, ys

  ! Declare variables which will be held as temporary values when
  ! implementing the matrix formula.
  real(kind = 8) :: xrT, xsT, yrT, ysT

  ! Declare variable which will be used to calculate the determinant.
  real(kind = 8) :: det

  ! Declare arrays which will hold the values for the derivatives
  ! ux and uy.
  real(kind = 8), dimension(:, :), allocatable :: ux, uy

  ! Declare variables which will hold the values for the partial derivatives
  ! rx, rs, sx, sy.
  real(kind = 8), dimension(:, :), allocatable :: rx, ry, sx, sy
  
  real(kind = 8), dimension(:,:), allocatable :: xc,yc

  nr = 30
  ns = 60
  
  ! Allocate memory for various arrays
  allocate(r(0:nr),s(0:ns),u(0:nr,0:ns),ur(0:nr,0:ns),us(0:nr,0:ns))
  allocate(xc(0:nr,0:ns),yc(0:nr,0:ns))
  allocate(err1(0:nr, 0:ns))

  ! Allocate memory for the arrays which will hold differentiated values.
  allocate(xr(0:nr, 0:ns), xs(0:nr, 0:ns), yr(0:nr, 0:ns), ys(0:nr, 0:ns))

  ! Allocate memory for the arrays which will hold the partial derivatives
  ! rx, ry, sx, sy.
  allocate(rx(0:nr, 0:ns), ry(0:nr, 0:ns), sx(0:nr, 0:ns), sy(0:nr, 0:ns))
  
  ! Allocate memory for the arrays which will hold the final partial
  ! derivatives.
  allocate(ux(0:nr, 0:ns), uy(0:nr, 0:ns))

  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  !! Part 4. !!!!!!!!!!!!!!!!!!!!
  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

  ! Now in order to assess the accuracy of this differentiation method,
  ! we are going to perform this algorithm for different spacing levels
  ! of hr and hs.
  t = 2
     hr = t/dble(nr)
     hs = t/dble(ns)
     do i = 0,nr
        r(i) = -1.d0 + dble(i)*hr
     end do
     do i = 0,ns
        s(i) = -1.d0 + dble(i)*hs
     end do

     do j = 0,ns
        do i = 0,nr
           xc(i,j) = x_coord(r(i),s(j))
           yc(i,j) = y_coord(r(i),s(j))
        end do
     end do
  
     call  printdble2d(xc,nr,ns,'x.txt')
     call  printdble2d(yc,nr,ns,'y.txt')
  
     do j = 0,ns
        do i = 0,nr
           u(i,j) = xc(i, j) + yc(i, j)
        end do
     end do
  
     ! Differentiate u in the r-direction
     do i = 0,ns
        call differentiate(u(0:nr,i),ur(0:nr,i),hr,nr)
     end do
     
     ! Differentiate u in the s-direction
     do i = 0,nr
        call differentiate(u(i,0:ns),us(i,0:ns),hs,ns)
     end do

     !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
     !! Part 2. !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
     !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  
     ! Differentiate x-coordinates in the r-direction.
     do i = 0, ns
        call differentiate(xc(0:nr, i), xr(0:nr, i), hr, nr)
     end do
  
     ! Differentiate x in the s-direction.
     do i = 0, nr
        call differentiate(xc(i, 0:ns), xs(i, 0:ns), hs, ns)
     end do

     ! Differentiate y in the r-direction.
     do i = 0, ns
        call differentiate(yc(0:nr, i), yr(0:nr, i), hr, nr)
     end do

     ! Differentiate y in the s-direction.
     do i = 0, nr
        call differentiate(yc(i, 0:ns), ys(i, 0:ns), hs, ns)
     end do

     !! Now that we have our partial derivative calculated (xr, xs, yr, ys), our
     !! next task is to use the matrix formula defined in the problem paper to
     !! calculate rx, ry, rx and sy.
     do i = 0, nr
        do j = 0, ns

           ! Assign respecive variables.
           xrT = xr(i, j)
           xsT = xs(i, j)
           yrT = yr(i, j)
           ysT = ys(i, j)

           ! Calculate determinant.
           det = xrT*ysT - xst*yrT

           ! Calculate partial derivatives rx, rs, sx, sy.
           rx(i, j) = ysT / det
           sx(i, j) = -yrT / det
           ry(i, j) = -xsT / det
           sy(i, j) = xrT / det

           ! Calculate the final partial derivatives ux and uy.
           ux(i, j) = ur(i, j)*rx(i, j) + us(i, j)*sx(i, j)
           uy(i, j) = ur(i, j)*ry(i, j) + us(i, j)*sy(i, j)

           ! Error approximation.
           err1(i, j) = sqrt(((ux(i, j) + uy(i, j)) - 4)**2)

        end do
     end do

     ! Write to .txt file.
     call printdble2d(ux, nr, ns, 'ux.txt')
     call printdble2d(uy, nr, ns, 'uy.txt')
     call printdble2d(err1, nr, ns, 'err1.txt')

     !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
     !! Part 3 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
     !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
     ! Error approximation.
  
end program hwk4
