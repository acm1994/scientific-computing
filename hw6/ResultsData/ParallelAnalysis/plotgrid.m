clc
clear all
close all
load x.txt 
load y.txt 

%plot(x,y,'k',x',y','k')
%axis equal
%print -depsc2 grid.eps

% Generate xT and yT (transpose of x and y, respectively).
xT = transpose(x);
yT = transpose(y);

% Since we will be using the pandas and matplotlib modules
% in Python to create all of our plots, we will have to add in
% a top row to denote the position of each column.
sizeX = size(x);
sizeY = size(y);
sizeXt = size(xT);
sizeYt = size(yT);

% x.
temp = zeros(sizeX(1) + 1, sizeX(2));
temp(1, :) = [1:(sizeX(2))];
temp(2:(sizeX(1) + 1), :) = x;
x = temp;

% y.
temp = zeros(sizeY(1) + 1, sizeY(2));
temp(1, :) = [1:(sizeY(2))];
temp(2:(sizeY(1) + 1), :) = y;
y = temp;

% xT.
temp = zeros(sizeXt(1) + 1, sizeXt(2));
temp(1, :) = [1:(sizeXt(2))];
temp(2:(sizeXt(1) + 1), :) = xT;
xT = temp;

% yT.
temp = zeros(sizeYt(1) + 1, sizeYt(2));
temp(1, :) = [1:(sizeYt(2))];
temp(2:(sizeYt(1) + 1), :) = yT;
yT = temp;

% Write all files to a .csv.
csvwrite('x.csv', x);
csvwrite('y.csv', y);
csvwrite('xT.csv', xT);
csvwrite('yT.csv', yT);
