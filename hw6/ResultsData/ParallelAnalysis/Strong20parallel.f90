program hwk4
  use omp_lib
  use xycoord   !use the module xycoord to set the mapping 
  implicit none
  integer :: nr,ns,i,j
  integer :: k ! For loop iterator for error calculation section.
  real(kind = 8) :: hr,hs
  real(kind = 8) :: t ! Update value.
  real(kind = 8) :: maxErr, maxJacob ! Maximum error and Jacobian.
  real(kind = 8), dimension(:), allocatable :: r,s
  real(kind = 8), dimension(:,:), allocatable :: u,ur,us
  integer, parameter :: out_unit = 20
  integer :: p ! For loop counter.
  real(kind = 8) :: time1, time2, timeA, timeB
  integer :: l ! Major do loop iterator.

  ! Allocate memory for the arrays which will hold partial derivatives.
  real(kind = 8), dimension(:, :), allocatable :: xr, xs, yr, ys

  ! Declare variables which will be held as temporary values when
  ! implementing the matrix formula.
  real(kind = 8) :: xrT, xsT, yrT, ysT

  ! Declare variable which will be used to calculate the determinant.
  real(kind = 8) :: det

  ! Array for f evalutation.
  real(kind = 8), dimension(:, :), allocatable :: f

  ! Array for error approximation.
  real(kind = 8), dimension(:, :), allocatable :: err

  ! Declare arrays which will hold the values for the derivatives
  ! ux and uy.
  real(kind = 8), dimension(:, :), allocatable :: ux, uy

  ! Declare variables which will hold the values for the partial derivatives
  ! rx, rs, sx, sy.
  real(kind = 8), dimension(:, :), allocatable :: rx, ry, sx, sy

  ! Arrays which will hold values for the x and y coordinates.
  real(kind = 8), dimension(:,:), allocatable :: xc,yc

  ! Array which will hold ux + uy.
  real(kind = 8), dimension(:, :), allocatable :: Uest

  ! Arrays which will hold uxExact and uyExact.
  real(kind = 8), dimension(:, :), allocatable :: uxExact, uyExact

  ! Jacobian array.
  real(kind = 8), dimension(:, :), allocatable :: jacob

  ! Effectiv h.
  real(kind = 8) :: hEff

  ! Size of arrays.
  nr = 20
  ns = 20

  ! Write file headers.
  open (unit = out_unit, file = "strong20parallel.csv", action = "write", status = "replace")
  write (out_unit,*) "threads,", "cpuTime,", "wallClockTime"

  ! MAIN do loop.
  do l = 1,16

     ! Set number of threads. 
     !$ call omp_set_num_threads(l)
 
     ! Allocate memory for various arrays
     allocate(r(0:nr),s(0:ns),u(0:nr,0:ns),ur(0:nr,0:ns),us(0:nr,0:ns))
     allocate(xc(0:nr,0:ns),yc(0:nr,0:ns))

     ! Allocate memory for the arrays which will hold differentiated values.
     allocate(xr(0:nr, 0:ns), xs(0:nr, 0:ns), yr(0:nr, 0:ns), ys(0:nr, 0:ns))

     ! Allocate memory for the arrays which will hold the partial derivatives
     ! rx, ry, sx, sy.
     allocate(rx(0:nr, 0:ns), ry(0:nr, 0:ns), sx(0:nr, 0:ns), sy(0:nr, 0:ns))
  
     ! Allocate memory for the arrays which will hold the final partial
     ! derivatives.
     allocate(ux(0:nr, 0:ns), uy(0:nr, 0:ns))

     ! Allocate memorry for the array which holds U = ux + uy.
     allocate(Uest(0:nr, 0:ns))

     ! Allocate memory for uxExact and uyExact.
     allocate(uxExact(0:nr, 0:ns), uyExact(0:nr, 0:ns))

     ! Memory for Jacobian
     allocate(jacob(0:nr, 0:ns))

     ! Memory for f evaluation.
     allocate(f(0:nr, 0:ns), err(0:nr, 0:ns))

     !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
     !! Part 4. !!!!!!!!!!!!!!!!!!!!
     !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

     ! Now in order to assess the accuracy of this differentiation method,
     ! we are going to perform this algorithm for different spacing levels
     ! of hr and hs.

     ! Begin timers.
     call cpu_time(time1)
     timeA = omp_get_wtime()
     
     t = 2
     hr = t/dble(nr)
     hs = t/dble(ns)
     !$omp parallel do private(i)
     do i = 0,nr
        r(i) = -1.d0 + dble(i)*hr
     end do
     !$omp end parallel do
     !$omp parallel do private(i)
     do i = 0,ns
        s(i) = -1.d0 + dble(i)*hs
     end do
     !$omp end parallel do

     !$omp parallel do private(i, j)
     do j = 0,ns
        do i = 0,nr
           xc(i,j) = x_coord(r(i),s(j))
           yc(i,j) = y_coord(r(i),s(j))
        end do
     end do
     !$omp end parallel do

     !$omp parallel do private(i, j)
     do j = 0,ns
        do i = 0,nr
           u(i,j) = sin(xc(i, j))*cos(yc(i, j))
        end do
     end do
     !$omp end parallel do
  
     ! Differentiate u in the r-direction
     !$omp parallel do private(i)
     do i = 0,ns
        call differentiate(u(0:nr,i),ur(0:nr,i),hr,nr)
     end do
     !$omp end parallel do
     
     ! Differentiate u in the s-direction
     !$omp parallel do private(i)
     do i = 0,nr
        call differentiate(u(i,0:ns),us(i,0:ns),hs,ns)
     end do
     !$omp end parallel do

     !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
     !! Part 2. !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
     !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  
     ! Differentiate x-coordinates in the r-direction.
     !$omp parallel do private(i)
     do i = 0, ns
        call differentiate(xc(0:nr, i), xr(0:nr, i), hr, nr)
     end do
     !$omp end parallel do
  
     ! Differentiate x in the s-direction.
     !$omp parallel do private(i)
     do i = 0, nr
        call differentiate(xc(i, 0:ns), xs(i, 0:ns), hs, ns)
     end do
     !$omp end parallel do

     ! Differentiate y in the r-direction.
     !$omp parallel do private(i)
     do i = 0, ns
        call differentiate(yc(0:nr, i), yr(0:nr, i), hr, nr)
     end do
     !$omp end parallel do

     ! Differentiate y in the s-direction.
     !$omp parallel do private(i)
     do i = 0, nr
        call differentiate(yc(i, 0:ns), ys(i, 0:ns), hs, ns)
     end do
     !$omp end parallel do

     !! Now that we have our partial derivative calculated (xr, xs, yr, ys), our
     !! next task is to use the matrix formula defined in the problem paper to
     !! calculate rx, ry, rx and sy.
     !$omp parallel do private(i, j)
     do i = 0, nr
        do j = 0, ns

           ! Assign respecive variables.
           xrT = xr(i, j)
           xsT = xs(i, j)
           yrT = yr(i, j)
           ysT = ys(i, j)

           ! Calculate determinant.
           det = xrT*ysT - xst*yrT

           ! Calculate partial derivatives rx, rs, sx, sy.
           rx(i, j) = ysT / det
           sx(i, j) = -yrT / det
           ry(i, j) = -xsT / det
           sy(i, j) = xrT / det

           ! Calculate the final partial derivatives ux and uy.
           ux(i, j) = ur(i, j)*rx(i, j) + us(i, j)*sx(i, j)
           uy(i, j) = ur(i, j)*ry(i, j) + us(i, j)*sy(i, j)

        end do
     end do
     !$omp end parallel do

     ! Now that we have our approximation, it is now in our interest
     ! to compute the exact value of u(x, y) for both the x and y
     ! coordinats. We know that uxExact(x, y) = cos(x)cos(y) and
     ! uyExact(x, y) = -sin(x)sin(y).
     !$omp parallel do private(i, j)
     do i = 0, nr
        do j = 0, ns

           ! Perform computation.
           uxExact(i, j) = cos(xc(i, j))*cos(yc(i, j))
           uyExact(i, j) = -sin(xc(i, j))*sin(yc(i, j))
           
        end do
     end do
     !$omp end parallel do

     ! Calculate Jacobian.
     !$omp parallel do private(i, j)
     do i = 0, nr
        do j = 0, ns

           ! Perform compuation.
           jacob(i, j) = xr(i, j)*ys(i, j) - xs(i, j)*yr(i, j)

        end do
     end do
     !$omp end parallel do

     ! Calculate error.
     !$omp parallel do private(i, j)
     do i = 0, nr
        do j = 0, ns

           ! Perform function evaluation (with Jacobian).
           f(i, j) = ((ux(i, j) + uy(i, j) - uxExact(i, j) - uyExact(i, j))**2)*jacob(i, j)

           ! Evaluate error.
           err(i, j) = f(i, j)*2
           err(i, j) = err(i, j)*2
           err(i, j) = sqrt(err(i, j))
           
        end do
     end do
     !$omp end parallel do

     ! Determine the maximum error.
     !$omp parallel do private(i, j)
     do i = 0, nr
        do j = 0, ns

          ! Check to see if we are on the first iteration.
          if (i == 0 .AND. j == 0) then
             maxErr = err(i, j)
             maxJacob = jacob(i, j)
          end if

          ! Otherwise, determine whether current iteration
          ! is greater than the current maximums.
          if (err(i, j) > maxErr) then
             maxErr = err(i, j)
          end if

          if (jacob(i, j) > maxJacob) then
             maxJacob = jacob(i, j)
          end if

       end do
    end do
    !$omp end parallel do
    
    ! Calculate hEff.
    hEff = sqrt(hs*hr*maxJacob)

    ! End timers.
    call cpu_time(time2)
    timeB = omp_get_wtime()
    
    ! Deallocate all memory.
    deallocate(r, s, ur, us, rx, ry, sx, sy, xc, yc, xr, xs, yr, ys, ux, uy, uEst, uxExact, uyExact, jacob, f, err, u)

    ! User output.
    write(*, *)  l, "threads: ", time2-time1, "cpu_time.", timeB-timeA, "wall clock time."

    ! Write to .csv file.
    write(out_unit, *) l, ",", time2-time1, ",", timeB-timeA
    
    ! Weak Scaling Analysis update. 
    !nr = sqrt(real(l+1))*200
    !ns = sqrt(real(l+1))*200
    !hs = 2/dble(ns)
    !hr = 2/dble(nr)
    
end do

 ! Close .csv file.
 close (out_unit)
    
end program hwk4

    
     
     
        
