=====================================
Parallelization Analysis Subdirectory
=====================================

1) This subdirectory will contain all of the .f90 code, .exe executable files and .csv datasets that are necessary to view our final results.
2) In addition to all of the files and their types just mentioned, all of the original .f90 and .mod files from Homework4 are present in this directory so that compilation may be performed if needed.
