==========================
Final Results Subdirectory
==========================

1) All of the directories here will hold the results for all parts of this homework assignment.


2) These parts include the original results from Homework 4, the parallel analysis results and some (unused in final report) results from implementing Stampede2.
