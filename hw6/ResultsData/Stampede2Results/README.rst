===========================
Stampede2 Results (OBSOLETE)
===========================

1) All of the files in this subdirectory will hold all of the jobs submitted to Stampede2 and all output generated from it.
2) These files will not be included in the final report as the parallel capabilities on the UNM Linux virtual machines were used to gather our results.
   
