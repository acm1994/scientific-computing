=================================================================
Parallel Computing 1: Integration and Differentiation With OpenMP
=================================================================

1) All of the files that were used in Homework 6 (including the final report) will be stored here.
  
2) For this repository, two subdirectories are present.
    
   a) OriginalCode: All of the code that was used in Homework 4 will be stored here (as we are going to be using the exact same code to demonstrate parallelization).
   b) ResultsData: All final data that was used and plotted in our final report will be stored here. Datasets will be stored in a .csv file format.

  
3) The final report is a .pdf file named ParallelComputing1.pdf.
