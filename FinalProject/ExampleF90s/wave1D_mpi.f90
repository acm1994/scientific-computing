program wave1D_mpi
  use mpi
  implicit none
  integer :: ierr, nprocs, myid
  integer :: status(MPI_STATUS_SIZE)
  
  integer, parameter :: nx = 100
  real(kind = 8) :: t_final = 2.d0
  integer :: i,j,nt,it
  
  integer :: px,ix_off ! the x-proc index and the offset
  integer :: p_left,p_right,px_max
  integer :: nxl       !this is the local size
  integer :: remx
  
  real(kind = 8) :: hx,t,dt,max_err_loc,max_err
  real(kind = 8), dimension(:),  allocatable :: x
  real(kind = 8), dimension(:, :), allocatable :: u,up,um,uxx,force,uex

  ! Initialize second dimension (computation, not MPI).
  real(kind = 8), dimension(:), allocatable :: y

  ! Initialize constants (w, kx, ky).
  real(kind = 8) :: w, kx, ky
  
  integer :: int_sum
  CHARACTER(7) :: char_step
  CHARACTER(7) :: char_id

  real(kind = 8) :: ums(1)
  
  real(kind=8) :: mpi_t0, mpi_t1, mpi_dt

  ! Split up the problem into nprocs copies. 
  call mpi_init(ierr)
  call mpi_comm_size(MPI_COMM_WORLD, nprocs, ierr)
  call mpi_comm_rank(MPI_COMM_WORLD, myid, ierr)

  call MPI_Barrier(MPI_COMM_WORLD,ierr)

  ! Begin computation timer. 
  mpi_t0=mpi_wtime()
  
  hx = 2.d0/dble(nx-1)

  ! Set constants.
  w = 10
  kx = 6
  ky = 4
  
  ! Label the processes from 1 to px_max
  px = myid + 1
  px_max = nprocs
  
  ! Split up the grid in the x-direction
  nxl = nx/px_max
  remx = nx-nxl*px_max
  if (px .le. remx) then
     nxl = nxl + 1
     ix_off = (px-1)*nxl
  else
     ix_off = (remx)*(nxl+1) + (px-(remx+1))*nxl ! Determine process offset. 
  end if
  call MPI_BARRIER(MPI_COMM_WORLD,ierr)
  call MPI_Reduce(nxl,int_sum,1,&
       MPI_INTEGER,MPI_SUM,&
       0,MPI_COMM_WORLD,ierr)
  if(myid == 0) then
     if (nx .ne. int_sum) then
        write(*,*) 'Something is wrong with the number of points in x-direction: ',&
             nx,int_sum
     end if
  end if
  
  ! Determine the neighbours of processor px
  p_left  = px-1 - 1
  p_right = px+1 - 1
  if (px .eq. px_max) p_right = MPI_PROC_NULL
  if (px .eq. 1) p_left = MPI_PROC_NULL  

  ! Allocate memory for various arrays
  allocate(u(0:(int(nx)-1), 0:nxl+1),up(0:(int(nx)-1), 0:nxl+1),um(0:(int(nx)-1), 0:nxl+1))
  allocate(x(0:nxl+1),uxx(0:(int(nx)-1), 0:nxl+1),force(0:(int(nx)-1), 0:nxl+1),uex(0:(int(nx)-1), 0:nxl+1))

  ! Allocate "y" vector. 
  allocate(y(0:(int(nx)-1)))
  
  ! Determine the portion of grid (x) that the current process will
  ! be responsible for.
  do i = 1,nxl
     x(i) = -1.d0 + dble(i-1+ix_off)*hx
  end do

  ! Create the spacing for the "y" vector.
  do i = 0, (int(nx)-1)
     y(i) = -1.d0 + i*hx
  end do
  
  ! send to left recieve from right
  call MPI_Sendrecv(x(1),1,MPI_DOUBLE_PRECISION,p_left,123,&
       x(nxl+1),1,MPI_DOUBLE_PRECISION,p_right,123,MPI_COMM_WORLD,status,ierr)
  ! send to right recieve from left
  call MPI_Sendrecv(x(nxl),1,MPI_DOUBLE_PRECISION,p_right,125,&
       x(0),1,MPI_DOUBLE_PRECISION,p_left,125,MPI_COMM_WORLD,status,ierr)
  call MPI_BARRIER(MPI_COMM_WORLD,ierr)

  t = 0.d0
  
  ! Compute the timestep
  dt = 0.9d0*hx      !this must satisfy the CFL condition
  nt = floor(t_final/dt)+1
  dt = t_final/dble(nt)
  
  WRITE(char_id,"(I7.7)") myid
  !call printdble1d(x(1:nxl),nxl,"./SOL/x"//trim(char_id)//".txt")

  ! Set up initial data including the ghost points
  do i = 0, (int(nx)-1)
     do j = 0, nxl+1
        call mms(ums,x(j),y(i),t-dt,w,kx,ky)
        um(i, j) = ums(1)
        call mms(ums,x(j),y(i),t,w,kx,ky)
        u(i, j) = ums(1)
     end do
  end do
  
  ! Do time stepping
  do it = 1, nt
     ! Communicate between processors
     ! send to left recieve from right
     call MPI_Sendrecv(u(0:(int(nx)-1), 1),nx,MPI_DOUBLE_PRECISION,p_left,125,&
          u(0:(int(nx)-1), nxl),nx,MPI_DOUBLE_PRECISION,p_right,125,MPI_COMM_WORLD,status,ierr)
     ! send to right recieve from left
     call MPI_Sendrecv(u(0:(int(nx)-1), nxl),nx,MPI_DOUBLE_PRECISION,p_right,123,&
          u(0:(int(nx)-1), 1),nx,MPI_DOUBLE_PRECISION,p_left,123,MPI_COMM_WORLD,status,ierr)
     call MPI_BARRIER(MPI_COMM_WORLD,ierr)
     ! Specify physical BC
     if (px .eq. px_max) then
        do i = 0, (int(nx)-1)
           call mms(ums,x(nxl),y(i),t,w,kx,ky)
           u(i, nxl) = ums(1)
        end do
        ! Compute top border.
        do j = 1, nxl
           call mms(ums, x(j), y(0), t, w, kx, ky)
           u(0, j) = ums(1)
        end do
        ! Compute bottom border.
        do j = 1, nxl
           call mms(ums, x(j), y(int(nx)-1), t, w, kx, ky)
           u(int(nx)-1, j) = ums(1)
        end do
     elseif (px .eq. 1) then
        do i = 0, (int(nx)-1)
           call mms(ums,x(1),y(i),t,w,kx,ky)
           u(i, 1) = ums(1)
        end do
        ! Compute top border.
        do j = 1, nxl
          call mms(ums, x(j), y(0), t, w, kx, ky)
          u(0, j) = ums(1)
        end do
        ! Compute bottom border.
        do j = 1, nxl
           call mms(ums, x(j), y(int(nx)-1), t, w, kx, ky)
           u(int(nx)-1, j) = ums(1)
        end do
     else
        do j = 1, nxl
           call mms(ums, x(j), y(int(nx)-1), t, w, kx, ky)
           u(int(nx)-1, j) = ums(1)
        end do
        do j = 1, nxl
           call mms(ums, x(j), y(0), t, w, kx, ky)
           u(0, j) = ums(1)
        end do
     end if
    
     ! Compute the right hand side
     call compute_uxx(uxx,u,nxl,hx,t,x,y, nx)
     call compute_forcing(force,nxl,t,x,y,w,kx,ky,nx)
     
     ! Compute the new solution up from u and um
         
     up(0:(int(nx)-1), 1:nxl) = 2.d0*u(0:(int(nx)-1), 1:nxl) - um(0:(int(nx)-1), 1:nxl) &
          + dt**2*(force(0:(int(nx)-1), 1:nxl) + uxx(0:(int(nx)-1), 1:nxl)) 
     ! Increment time and swap the solution at different time levels
     t = t+dt
     um = u
     u  = up

     
     call MPI_BARRIER(MPI_COMM_WORLD,ierr)
     ! Update physical BC
     if (px .eq. px_max) then
        do i = 0, (int(nx-1))
           call mms(ums,x(nxl),y(i),t,w,kx,ky)
           u(i, nxl) = ums(1)
        end do
        ! Compute top border.
        do j = 1, nxl
           call mms(ums, x(j), y(int(nx)-1), t, w, kx, ky)
           u(int(nx)-1, j) = ums(1)
        end do
        ! Compute bottom border.
        do j = 1, nxl
           call mms(ums, x(j), y(0), t, w, kx, ky)
           u(0, j) = ums(1)
        end do
     elseif (px .eq. 1) then
        do i = 0, (int(nx)-1)
           call mms(ums,x(1),y(i),t,w,kx,ky)
           u(i, 1) = ums(1)
        end do
        ! Compute bottom border. 
        do j = 1, nxl
           call mms(ums, x(j), y(0), t, w, kx, ky)
           u(0, j) = ums(1)
        end do
        ! Compute top border.
        do j = 1, nxl
           call mms(ums, x(j), y(int(nx)-1), t, w, kx, ky)
           u(int(nx)-1, j) = ums(1)
        end do
     else
        ! Compute bottom border.
        do j = 1, nxl
           call mms(ums, x(j), y(0), t, w, kx, ky)
           u(0, j) = ums(1)
        end do
        ! Compute top border.
        do j = 1, nxl
           call mms(ums, x(j), y(int(nx)-1), t, w, kx ,ky)
           u(int(nx)-1, j) = ums(1)
        end do
     end if
     
     WRITE(char_id,"(I7.7)") myid
     WRITE(char_step,"(I7.7)") it
     !call printdble1d(u(1:nxl),nxl,"./SOL/sol"//trim(char_step)//"_"//trim(char_id)//".txt")
     
     ! Compute the max error at time t
     max_err_loc = maxval(abs(u(0:(int(nx)-1), 1:nxl)-uex))
     call MPI_Reduce(max_err_loc,max_err,1,&
        MPI_DOUBLE_PRECISION,MPI_MAX,&
        0,MPI_COMM_WORLD,ierr) 
  end do
  if(myid.eq.0) write(*,*) "At time t=", t, "the max error is ", max_err

  call MPI_Barrier(MPI_COMM_WORLD,ierr)
  mpi_t1=mpi_wtime()
  mpi_dt=mpi_t1-mpi_t0
  call mpi_finalize(ierr)

  if(myid.eq.0) write(*,*) "With", nprocs, "  nodes the wall clock time = ", mpi_dt, " sec."
  
end program wave1D_mpi


SUBROUTINE mms(u,x,y,t,w,kx,ky)
! This subroutine returns the manufactured solution in the array u
! at a given point (x,y,t)
      
      DOUBLE PRECISION x,y,t
      DOUBLE PRECISION u(1)

      ! Initialize constants.
      real(kind = 8) :: w, kx, ky

      !t2 = 3.141592653589793D0*x
      !t3 = cos(t2)
      !t4 = 3.141592653589793D0*y
      !t5 = cos(t4)
      !t6 = 3.141592653589793D0*t
      !t7 = cos(t6)
      !u = t3*t5*t7

      u = sin(w*t-kx*x)*sin(ky*y)
      
END SUBROUTINE mms


SUBROUTINE compute_forcing(force,nx,t,x, y,w,kx,ky, nxY)
  implicit none
  integer :: nx,nxY
  real(kind = 8) :: force(nxY, nx+1),t,x(nx+1), y(nxY), w, kx, ky
  integer :: i, j
  
  do i = 1, nxY
     do j = 2, nx
        force(i, j) = -(w**2-kx**2-ky**2)*sin(w*t-kx*x(j))*sin(ky*y(i))
     end do
  end do
END SUBROUTINE compute_forcing

SUBROUTINE compute_uxx(uxx,u,nx,hx,t,x,y, nxY)
  implicit none
  integer :: nx, nxY
  real(kind = 8) :: u(nxY, nx+1),x(nx+1),hx,uxx(nxY, nx+1),t, y(nxY)
  integer :: i, j
  real(kind = 8) :: h2i

  !h2i = 1.d0/hx/hx
  do i = 2, nxY-1
     do j = 3, nx-2
        uxx(i,j) = (1/hx**2)*u(i,j+1)+(1/hx**2)*u(i,j-1)+(1/hx**2)*u(i+1,j)+(1/hx**2)*u(i-1,j)-((2/hx**2)+(2/hx**2))*u(i,j)
     end do
  end do
END SUBROUTINE compute_uxx


subroutine printdble1d(u,nx,str)
  implicit none
  integer, intent(in) :: nx
  real(kind = 8), intent(in) :: u(nx)
  character(len=*), intent(in) :: str
  integer :: i
  open(2,file=trim(str),status='unknown')
  do i=1,nx,1
     write(2,fmt='(E24.16)',advance='no') u(i)
  end do
  close(2)
end subroutine printdble1d
   
