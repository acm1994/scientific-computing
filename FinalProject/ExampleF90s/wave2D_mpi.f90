program wave1D_mpi
  use mpi
  implicit none
  integer :: ierr, nprocs, myid
  integer :: status(MPI_STATUS_SIZE)
  
  integer, parameter :: nx = 100, ny = 100
  real(kind = 8) :: t_final = 2.d0
  integer :: i,j,ntx,it,nty
  
  integer :: px,ix_off, py, iy_off ! the x-proc index and the offset
  integer :: px_left,px_right,px_max, py_left, py_right, py_max
  integer :: nxl, nyl       !this is the local size
  integer :: remx, remy
  
  real(kind = 8) :: hx,t,dtx,max_err_loc,max_err,x_err, y_err, hy, dty
  real(kind = 8), dimension(:),  allocatable :: x,ux,upx,umx,uxx,forcex,uex,y,uyy,uy,upy,umy,forcey,uey
  integer :: int_sumx, int_sumy
  CHARACTER(7) :: char_step
  CHARACTER(7) :: char_id

  real(kind = 8) :: ums(1)
  
  real(kind=8) :: mpi_t0, mpi_t1, mpi_dt

  call mpi_init(ierr)
  call mpi_comm_size(MPI_COMM_WORLD, nprocs, ierr)
  call mpi_comm_rank(MPI_COMM_WORLD, myid, ierr)

  call MPI_Barrier(MPI_COMM_WORLD,ierr)
  mpi_t0=mpi_wtime()
  
  hx = 2.d0/dble(nx-1)
  hy = 2.d0/dble(ny-1) ! Added in ny.
  
  ! Label the processes from 1 to px_max
  px = myid + 1
  px_max = nprocs

  ! Label the processes from 1 to py_max.
  py = myid + 1
  py_max = nprocs
  
  ! Split up the grid in the x-direction
  nxl = nx/px_max

  ! Split up the grid in the y-direction.
  nyl = ny/py_max
  
  remx = nx-nxl*px_max
  remy = ny-nyl*py_max
  if (px .le. remx) then
     nxl = nxl + 1
     ix_off = (px-1)*nxl
  else
     ix_off = (remx)*(nxl+1) + (px-(remx+1))*nxl
  end if

  call MPI_BARRIER(MPI_COMM_WORLD,ierr)
  call MPI_Reduce(nxl,int_sumx,1,&
       MPI_INTEGER,MPI_SUM,&
       0,MPI_COMM_WORLD,ierr)
  if(myid == 0) then
     if (nx .ne. int_sumx) then
        write(*,*) 'Something is wrong with the number of points in x-direction: ',&
             nx,int_sumx
     end if
       end if

  ! Y-coordinates (if loop).
  if (py .le. remy) then
     nyl = nyl + 1
     iy_off = (py-1)*nyl
  else
     iy_off = (remy)*(nyl+1) + (py - (remy+1))*nyl
  end if

  call MPI_BARRIER(MPI_COMM_WORLD,ierr)
  call MPI_Reduce(nyl,int_sumy,1,&
       MPI_INTEGER,MPI_SUM,&
       0,MPI_COMM_WORLD,ierr)
  if(myid == 0) then
     if (ny .ne. int_sumy) then
        write(*,*) 'Something is wrong with the number of points in x-direction: ',&
             ny,int_sumy
     end if
  end if
  
  ! Determine the neighbours of processor px (x-coordinates).
  px_left  = px-1 - 1
  px_right = px+1 - 1
  if (px .eq. px_max) px_right = MPI_PROC_NULL
  if (px .eq. 1) px_left = MPI_PROC_NULL

  ! Determine the neighbours of processor py (y-coordinates).
  py_left = py-1 - 1
  py_right = py+1 - 1
  if (py .eq. py_max) py_right = MPI_PROC_NULL
  if (py .eq. 1) py_left = MPI_PROC_NULL

  ! Allocate memory for various arrays (x-coordinates)
  allocate(ux(0:nxl+1),upx(0:nxl+1),umx(0:nxl+1))
  allocate(x(0:nxl+1),uxx(1:nxl),forcex(1:nxl),uex(1:nxl))
  do i = 1,nxl
     x(i) = -1.d0 + dble(i-1+ix_off)*hx
  end do

  ! Allocate memory for various arrays (y-coordinates)
  allocate(uy(0:nyl+1), upy(0:nyl+1),umy(0:nyl+1))
  allocate(y(0:nyl+1),uyy(1:nyl),forcey(1:nyl),uey(1:nyl))
  do i = 1,nyl
     y(i) = -1.d0 + dble(i-1+iy_off)*hy
  end do
  
  ! send to left recieve from right (x-coordinates)
  call MPI_Sendrecv(x(1),1,MPI_DOUBLE_PRECISION,px_left,123,&
       x(nxl+1),1,MPI_DOUBLE_PRECISION,px_right,123,MPI_COMM_WORLD,status,ierr)
  ! send to right recieve from left (x-coordinates)
  call MPI_Sendrecv(x(nxl),1,MPI_DOUBLE_PRECISION,px_right,125,&
       x(0),1,MPI_DOUBLE_PRECISION,px_left,125,MPI_COMM_WORLD,status,ierr)
  call MPI_BARRIER(MPI_COMM_WORLD,ierr)

  ! Send to the left, recieve from the right (y-coordinates).
  call MPI_Sendrecv(y(1),1,MPI_DOUBLE_PRECISION,py_left,122,&
       y(nyl+1),1,MPI_DOUBLE_PRECISION,py_right,122,MPI_COMM_WORLD,status,ierr)
  ! Send to the right, recieve from the left (y-coordinates).
  call MPI_Sendrecv(y(nyl),1,MPI_DOUBLE_PRECISION,py_right,124,&
       y(0),1,MPI_DOUBLE_PRECISION,py_left,124,MPI_COMM_WORLD,status,ierr)
  call MPI_BARRIER(MPI_COMM_WORLD,ierr)
  

  t = 0.d0
  
  ! Compute the timestep (x-coordinates)
  dtx = 0.9d0*hx      !this must satisfy the CFL condition
  ntx = floor(t_final/dtx)+1
  dtx = t_final/dble(ntx)

  ! Compute the timestep (y-coordinates).
  dty = 0.9d0*hy
  nty = floor(t_final/dty)+1
  dty = t_final/dble(nty)
  
  WRITE(char_id,"(I7.7)") myid
  call printdble1d(x(1:nxl),nxl,"./SOL/x"//trim(char_id)//".txt")
  call printdble1d(y(1:nyl),nyl,"./SOL/y"//trim(char_id)//".txt")

  ! Set up initial data including the ghost points (x-coordinates).
  do i = 0,nxl+1
     call mms(ums,x(i),0.d0,t-dtx)
     umx(i) = ums(1)
     call mms(ums,x(i),0.d0,t)
     ux(i) = ums(1)
  end do

  t = 0.d0

  ! Set up initial data including the ghost points (y-coordinates).
  do i = 0,nyl+1
     call mms(ums,y(i),0.d0,t-dty)
     umy(i) = ums(1)
     call mms(ums,x(i),0.d0,t)
     uy(i) = ums(1)
  end do
  
  ! Do time stepping (x-coordinates).
  do it = 1,ntx
     ! Communicate between processors
     ! send to left recieve from right
     call MPI_Sendrecv(ux(1),1,MPI_DOUBLE_PRECISION,px_left,123,&
          ux(nxl+1),1,MPI_DOUBLE_PRECISION,px_right,123,MPI_COMM_WORLD,status,ierr)
     ! send to right recieve from left
     call MPI_Sendrecv(ux(nxl),1,MPI_DOUBLE_PRECISION,px_right,125,&
          ux(0),1,MPI_DOUBLE_PRECISION,px_left,125,MPI_COMM_WORLD,status,ierr)
     call MPI_BARRIER(MPI_COMM_WORLD,ierr)
     ! Specify physical BC
     if (px .eq. px_max) then
        call mms(ums,x(nxl),0.d0,t)
        ux(nxl) = ums(1)
     end if
     if (px .eq. 1) then
        call mms(ums,x(1),0.d0,t)
        ux(1) = ums(1)
     end if
     ! Compute the right hand side
     call compute_uxx(uxx,ux,nxl,hx,t,x)
     call compute_forcing(forcex,nxl,t,x)
     ! Compute the new solution up from u and um
     upx(1:nxl) = 2.d0*ux(1:nxl) - umx(1:nxl) + dtx**2*(uxx+forcex) 
     ! Increment time and swap the solution at different time levels
     t = t+dtx
     umx = ux
     ux  = upx
     call MPI_BARRIER(MPI_COMM_WORLD,ierr)
     ! Update physical BC
     if (px .eq. px_max) then
        call mms(ums,x(nxl),0.d0,t)
        ux(nxl) = ums(1)
     end if
     if (px .eq. 1) then
        call mms(ums,x(1),0.d0,t)
        ux(1) = ums(1)
     end if
     
     WRITE(char_id,"(I7.7)") myid
     WRITE(char_step,"(I7.7)") it
     !call printdble1d(u(1:nxl),nxl,"./SOL/sol"//trim(char_step)//"_"//trim(char_id)//".txt")
     
     ! Compute the exact solution
     do i = 1,nxl
        call mms(ums,x(i),0.d0,t)
        uex(i) = ums(1)
     end do
     
     ! Compute the max error at time t
     max_err_loc = maxval(abs(ux(1:nxl)-uex))
     call MPI_Reduce(max_err_loc,x_err,1,&
          MPI_DOUBLE_PRECISION,MPI_MAX,&
          0,MPI_COMM_WORLD,ierr) 
  end do
  if(myid.eq.0) write(*,*) "At time t=", t, "the max(x) error is ", x_err

  t = 0.d0
  
  ! Do time stepping (y-coordinates).
  do it = 1,nty
     ! Communicate between processors
     ! send to left recieve from right
     call MPI_Sendrecv(uy(1),1,MPI_DOUBLE_PRECISION,py_left,122,&
          uy(nyl+1),1,MPI_DOUBLE_PRECISION,py_right,122,MPI_COMM_WORLD,status,ierr)
     ! send to right recieve from left
     call MPI_Sendrecv(uy(nyl),1,MPI_DOUBLE_PRECISION,py_right,124,&
          uy(0),1,MPI_DOUBLE_PRECISION,py_left,124,MPI_COMM_WORLD,status,ierr)
     call MPI_BARRIER(MPI_COMM_WORLD,ierr)
     ! Specify physical BC
     if (py .eq. py_max) then
        call mms(ums,y(nyl),0.d0,t)
        uy(nyl) = ums(1)
     end if
     if (py .eq. 1) then
        call mms(ums,y(1),0.d0,t)
        uy(1) = ums(1)
     end if
     ! Compute the right hand side
     call compute_uxx(uyy,uy,nyl,hy,t,y)
     call compute_forcing_y(forcey,nyl,t,y)
     ! Compute the new solution up from u and um
     upy(1:nyl) = 2.d0*uy(1:nyl) - umy(1:nyl) + dty**2*(uyy+forcey)
     ! Increment time and swap the solution at different time levels
     t = t+dty
     umy = uy
     uy  = upy
     call MPI_BARRIER(MPI_COMM_WORLD,ierr)
     ! Update physical BC
     if (py .eq. py_max) then
        call mms(ums,y(nyl),0.d0,t)
        uy(nyl) = ums(1)
     end if
     if (py .eq. 1) then
        call mms(ums,y(1),0.d0,t)
        uy(1) = ums(1)
     end if

     WRITE(char_id,"(I7.7)") myid
     WRITE(char_step,"(I7.7)") it
     !call printdble1d(u(1:nxl),nxl,"./SOL/sol"//trim(char_step)//"_"//trim(char_id)//".txt")

     ! Compute the exact solution
     do i = 1,nyl
        call mms(ums,x(i),0.d0,t)
        uey(i) = ums(1)
     end do
     
     ! Compute the max error at time t
     max_err_loc = maxval(abs(uy(1:nyl)-uey))
     call MPI_Reduce(max_err_loc,y_err,1,&
          MPI_DOUBLE_PRECISION,MPI_MAX,&
          0,MPI_COMM_WORLD,ierr)
  end do
    if(myid.eq.0) write(*,*) "At time t=", t, "the max(y) error is ", y_err

  call MPI_Barrier(MPI_COMM_WORLD,ierr)
  mpi_t1=mpi_wtime()
  mpi_dt=mpi_t1-mpi_t0
  call mpi_finalize(ierr)

  if(myid.eq.0) write(*,*) "With", nprocs, "  nodes the wall clock time = ", mpi_dt, " sec."
  
end program wave1D_mpi


SUBROUTINE mms(u,x,y,t)
! This subroutine returns the manufactured solution in the array u
! at a given point (x,y=0,t)
      
      DOUBLE PRECISION x,y,t
      DOUBLE PRECISION u(1)

      t2 = 3.141592653589793D0*x
      t3 = cos(t2)
      t4 = 3.141592653589793D0*y
      t5 = cos(t4)
      t6 = 3.141592653589793D0*t
      t7 = cos(t6)
      u = t3*t5*t7
END SUBROUTINE mms


SUBROUTINE compute_forcing(force,nx,t,x)
  implicit none
  integer :: nx
  real(kind = 8) :: force(1:nx),t,x(0:nx+1)
  integer :: i
  
  do i = 1,nx
     force(i) = 0
  end do
END SUBROUTINE compute_forcing

SUBROUTINE compute_forcing_y(force, ny, t, y)
  implicit none
  integer :: ny
  real(kind = 8) :: force(1:ny), t, y(0:ny+1)
  integer :: i

  do i = 0, ny
     force(i) = 0
  end do
end subroutine compute_forcing_y



SUBROUTINE compute_uxx(uxx,u,nx,hx,t,x)
  implicit none
  integer :: nx
  real(kind = 8) :: u(0:nx+1),x(0:nx+1),hx,uxx(1:nx),t
  integer :: i
  real(kind = 8) :: h2i

  h2i = 1.d0/hx/hx
  do i = 1,nx
     uxx(i) = h2i*(u(i+1)-2.d0*u(i)+u(i-1))
  end do
END SUBROUTINE compute_uxx


subroutine printdble1d(u,nx,str)
  implicit none
  integer, intent(in) :: nx
  real(kind = 8), intent(in) :: u(nx)
  character(len=*), intent(in) :: str
  integer :: i
  open(2,file=trim(str),status='unknown')
  do i=1,nx,1
     write(2,fmt='(E24.16)',advance='no') u(i)
  end do
  close(2)
end subroutine printdble1d
   
