program wave2Dserial
  use omp_lib
  implicit none
  
  ! Declaration of variables
  real(kind = 8), dimension(:), allocatable :: HXarray ! Increasing grid points.
  real(kind = 8), dimension(:), allocatable :: E, H
  real(kind = 8) :: nx, ny, hx, hy ! Grid points.
  integer :: i ! Main for loop iterator.
  integer :: j, k ! Iterator for creating the x and y arrays.
  integer :: j2, k2 ! Iterators for subloops.
  real(kind = 8) :: t_final ! End time.
  real(kind = 8) :: dt ! Time step increment.
  real(kind = 8) :: nt ! Number of time steps.
  real(kind = 8) :: t ! Time step.
  real(kind = 8) :: w, kx, ky ! Variables in actual equation.
  real(kind = 8) :: maxVal ! Maximum value.
  real(kind = 8), dimension(:), allocatable :: x, y
  real(kind = 8), dimension(:, :), allocatable :: u0, u1, u2, uEx, uError ! Arrays to two time-steps.
  real(kind = 8), dimension(:, :), allocatable :: f ! Array for f.
  real(kind = 8), dimension(:, :), allocatable :: L
  real(kind = 8), dimension(:, :), allocatable :: force ! Forcing array.
  real(kind = 8) :: time1, time2, timeA, timeB

  ! Allocate HXarray and set up its values.
  allocate(HXarray(0:4))
  HXarray(0) = 11
  HXarray(1) = 11
  HXarray(2) = 11
  HXarray(3) = 11
  HXarray(4) = 11

  ! Allocate H and E arrays.
  allocate(H(0:4), E(0:4))

  ! Declare equation variables (will be constant throughout).
  w = 10
  kx = 6
  ky = 4

  ! Begin main computational for loop.
  do i = 0,  (size(HXarray) - 1)

     ! Array size. 
     ! Determine nx and ny (assume nx = ny).
     nx = HXarray(i)
     ny = nx

     ! Spacing.
     ! Determine hx and hy (again, hx = hy since nx = ny).
     hx = 2 / (nx - 1)
     hy = 2 / (ny - 1)

     ! Place hx in the H array.
     H(i) = hx

     ! Set up the x and y grids.
     allocate(x(0:(int(nx)-1)), y(0:(int(ny)-1)))
     do j = 0, (int(nx)-1)

        ! Set up x and y.
        x(j) = -1 + j*hx
        y(j) = -1 + j*hy

     end do
     
     ! Create time step intervals.
     t_final = 2
     dt = 0.5*hx
     nt = floor(t_final / dt) + 1
     dt = t_final / nt

     ! Set up initial data (i.e., obtain u0 and u1).
     ! Allocate memory.
     allocate(u0(0:(int(nx)-1), 0:(int(nx)-1)))
     allocate(f(0:(int(nx)-1), 0:(int(nx)-1)))
     allocate(L(0:(int(nx)-1), 0:(int(nx)-1)))
     allocate(force(0:(int(nx)-1), 0:(int(nx)-1)))

     call cpu_time(time1)
     timeA = omp_get_wtime()
     
     call uExact(u0, x, y, 0.d0, size(x), size(x), w, kx, ky)

     ! Calculate f.
     call f2(f, x, y, w, kx, ky, size(x))
     
     ! Calculate Laplacian.
     call laplacian(L, u0, hx, hy, size(x))

     ! Compute forcing.
     call forcing(force, 0.d0, x, y, w, kx, ky, size(x))

     ! Compute u1.
     allocate(u1(0:(int(nx)-1), 0:(int(nx)-1)))
     do j = 0, (int(nx)-1)
        do k = 0, (int(nx)-1)
           u1(j, k) = u0(j, k)+dt*f(j, k)+0.5*(dt**2)*(L(j, k)+force(j, k))
        end do
     end do

     ! Update BC.
     do j = 0, (int(nx)-1)
        u1(0, j) = sin(w*dt-kx*x(j))*sin(ky*y(0))
     end do
     do j = 0, (int(nx)-1)
        u1((int(nx)-1), j) = sin(w*dt-kx*x(j))*sin(ky*y(int(nx)-1))
     end do
     do j = 0, (int(nx)-1)
        u1(j, 0) = sin(w*dt-kx*x(0))*sin(ky*y(j))
     end do
     do j = 0, (int(nx)-1)
        u1(j, (int(nx)-1)) = sin(w*dt-kx*x(int(nx)-1))*sin(ky*y(j))
     end do

     ! Allocate u2.
     allocate(u2(0:(int(nx)-1), 0:(int(nx)-1)))

     ! Begin subsequent computations.
     do k = 0, (int(nt)-2)

        t = (k+1)*dt

        ! Obtain right-hand side.
        call forcing(force, t, x, y, w, kx, ky, size(x))
        call laplacian(L, u1, hx, hy, size(x))


        ! March in time.
        do j2  = 0, (int(nx)-1)
           do k2 = 0, (int(nx)-1)
              u2(j2, k2) = 2*u1(j2, k2)-u0(j2, k2)+(dt**2)*(L(j2, k2)+force(j2, k2))
           end do
        end do

        ! Update BC.
        do j2 = 0, (int(nx)-1)
           u2(0, j2) = sin(w*(dt+t)-kx*x(j2))*sin(ky*y(0))
        end do
        do j2 = 0, (int(nx)-1)
           u2((int(nx)-1), j2) = sin(w*(dt+t)-kx*x(j2))*sin(ky*y(int(nx)-1))
        end do
        do j2 = 0, (int(nx)-1)
           u2(j2, 0) = sin(w*(dt+t)-kx*x(0))*sin(ky*y(j2))
        end do
        do j2 = 0, (int(nx)-1)
           u2(j2, (int(nx)-1)) = sin(w*(dt+t)-kx*x(int(nx)-1))*sin(ky*y(j2))
        end do

        ! Switch solution at different time intervals.
        u0 = u1
        u1 = u2

     end do

        do k2 = 0, int(nx)-1
                  do j2 = 0, int(nx)-1
                              print *, force(k2,j2)
                                        end do
                                                end do
                                                print *, "SPACE"

                                                        do k2 = 0, int(nx)-1
                                                                  do j2 = 0, int(nx)-1
                                                                              print *, u1(k2,j2)
                                                                                        end do
                                                                                                end do


     ! Compute the exact solution at t_final.
     allocate(uEx(0:(int(nx)-1), 0:(int(nx)-1)))
     allocate(uError(0:(int(nx)-1), 0:(int(nx)-1)))
     call uExact(uEx, x, y, t_final, size(x), size(x), w, kx, ky)
     maxVal = 0
     do j = 0, (int(nx)-1)
        do k = 0, (int(nx)-1)
           uError(j, k) = abs(uEx(j, k)-u2(j, k))
        end do
     end do

     do j = 0, (int(nx)-1)
        do k = 0, (int(nx)-1)
           if (uError(j, k) > maxVal) then
              maxVal = uError(j, k)
           end if
        end do
     end do

     ! Store error in E.
     E(i) = maxVal
     
     ! Deallocate all arrays.
     deallocate(x, y, u0, f, L, force, u1, u2, uEx, uError)
  end do

  call cpu_time(time2)
  timeB = omp_get_wtime()

  ! Print out all values.
  do j = 0, (size(HXarray)-1)
     print *, 'h:', H(j), 'error:', E(j)
  end do

  print*, time2-time1, "CPU time,", timeB-timeA, "wall clock time."

end program wave2Dserial

! Subroutine which calcultes the exact value of u.
! IMPORTANT: This subroutine is written under the assumption that
!            u is square dimension. Do not attempt to use a
!            array with differing dimensions. 
subroutine uExact(u, x, y, t, usize, xsize, w, kx, ky)
  implicit none

  ! Declaration of variables.
  integer, intent(in) :: usize, xsize ! Size of all arrays.
  real(kind = 8), intent(out) :: u(usize, usize) ! Final output.
  real(kind = 8) :: x(xsize), y(xsize), t, w, kx, ky
  integer :: i, j ! Iterators. 

  ! Perform calculation.
  do i = 1, usize
     do j = 1, usize
        u(i, j) = sin(w*t-kx*x(j))*sin(ky*y(i))
     end do
  end do
  
end subroutine uExact

! Function which will calculate uExact at a particular point.
! WARNING: Assumption of nx = ny.
real(kind = 8) function uExactF(x, y, t, xLoc, yLoc, n, w, kx, ky)

  implicit none

  ! Declaration of variables.
  integer, intent(in) :: n, xLoc, yLoc
  real(kind = 8), intent(in) :: x(n), y(n), w, kx, ky, t

  ! Computation.
  uExactF = sin(w*t-kx*x(xLoc))*sin(ky*y(yLoc))

end function uExactF

! Subroutine which will compute f on our wave at each grid point.
! WARNING: Just as with uExact, this subroutine will run off of the assumption
! of nx = ny.
subroutine f2(fa, x, y, w, kx, ky, n)

  implicit none

  ! Declaration of variables & arrays.
  integer, intent(in) :: n
  real(kind = 8), intent(out) :: fa(n, n) ! Final output.
  real(kind = 8) :: x(n), y(n), w, kx, ky
  integer :: i, j ! Iterators.

  ! Perform calculation.
  do i = 1, n
     do j = 1, n
       fa(i, j) = w*cos(kx*x(j))*sin(ky*y(i))
     end do
  end do
  
end subroutine f2

! Subroutine which will compute the Laplacian.
! WARNING: Assumption of nx = ny.
subroutine laplacian(L, u, hx, hy, n)

  implicit none

  ! Declaration of variables & arrays.
  integer, intent(in) :: n
  real(kind = 8), intent(in) :: u(n, n)
  real(kind = 8), intent(out) :: L(n, n) ! Final output.
  real(kind = 8) :: hx, hy
  integer :: i, j ! Iterators.

  ! Perform computation.
  do i = 2, (n-1)
     do j = 2, (n-1)
        L(i, j)=(1/hx**2)*u(i,j+1)+(1/hx**2)*u(i,j-1)+(1/hy**2)*u(i+1,j)+(1/hy**2)*u(i-1,j)-((2/hx**2)+(2/hx**2))*u(i,j)
     end do
  end do
  
end subroutine laplacian

! Subroutine which will compute the forcing for the entire grid.
! WARNING: Assumption of nx = ny.
subroutine forcing(force, t, x, y, w, kx, ky, n)

  implicit none

  ! Declaration of variables.
  integer, intent(in) :: n
  real(kind = 8), intent(in) :: w, kx, ky
  real(kind = 8), intent(in) :: x(n), y(n)
  real(kind = 8) :: t
  real(kind = 8), intent(out) :: force(n, n)
  integer :: i, j

  ! Perform computation.
  do i = 1, n
     do j = 1, n
        force(i, j) = -(w**2-kx**2-ky**2)*sin(w*t-kx*x(j))*sin(ky*y(i))
     end do
  end do
  
end subroutine forcing

! Example.
subroutine f(n, r)

  implicit none
 
  integer, intent(in) :: n
  real(kind = 8), intent(out) :: r(n)
  integer :: i
  do i = 1, n
     r(i) = r(i) * 2
  end do

end subroutine f



