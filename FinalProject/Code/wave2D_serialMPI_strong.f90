program wave2Dserial
  use mpi
  use omp_lib
  implicit none

  ! Declaration of MPI-related variables.
  integer :: ierr, nprocs, myid
  integer :: status(MPI_STATUS_SIZE)
  real(kind=8) :: mpi_t0, mpi_t1, mpi_dt
  integer :: px,ix_off ! the x-proc index and the offset
  integer :: p_left,p_right,px_max
  integer :: remx
  integer :: int_sum
  CHARACTER(7) :: char_step
  CHARACTER(7) :: char_id
  
  ! Declaration of variables
  real(kind = 8), dimension(:), allocatable :: HXarray ! Increasing grid points.
  real(kind = 8), dimension(:), allocatable :: E, H
  real(kind = 8) :: nx, ny, hx, hy ! Grid points.
  integer :: i ! Main for loop iterator.
  integer :: j, k ! Iterator for creating the x and y arrays.
  integer :: j2, k2 ! Iterators for subloops.
  real(kind = 8) :: t_final ! End time.
  real(kind = 8) :: dt ! Time step increment.
  real(kind = 8) :: nt ! Number of time steps.
  real(kind = 8) :: t ! Time step.
  real(kind = 8) :: w, kx, ky ! Variables in actual equation.
  real(kind = 8) :: maxVal ! Maximum value.
  real(kind = 8), dimension(:), allocatable :: x, y
  real(kind = 8), dimension(:, :), allocatable :: u0, u1, u2, uEx, uError ! Arrays to two time-steps.
  real(kind = 8), dimension(:, :), allocatable :: f ! Array for f.
  real(kind = 8), dimension(:, :), allocatable :: L
  real(kind = 8), dimension(:, :), allocatable :: force ! Forcing array.
  real(kind = 8) :: err ! Error.
  integer :: nxl

  ! Declare equation variables (will be constant throughout).
  w = 10
  kx = 6
  ky = 4

  call mpi_init(ierr)
  call mpi_comm_size(MPI_COMM_WORLD, nprocs, ierr)
  call mpi_comm_rank(MPI_COMM_WORLD, myid, ierr)
  call MPI_BARRIER(MPI_COMM_WORLD,ierr)
  mpi_t0 = mpi_wtime()
  
  ! Array size. 
  ! Determine nx and ny (assume nx = ny).
  nx = real(int(200*sqrt(4.d0)))
  ny = nx

  ! Spacing.
  ! Determine hx and hy (again, hx = hy since nx = ny).
  hx = 2 / (nx - 1)
  hy = 2 / (ny - 1)

  ! Label the processes from 1 to px_max
  px = myid + 1
  px_max = nprocs

  ! Split up the grid in the x-direction
  nxl = nx/px_max
  remx = nx-nxl*px_max
  if (px .le. remx) then
     nxl = nxl + 1
     ix_off = (px-1)*nxl
  else
     ix_off = (remx)*(nxl+1) + (px-(remx+1))*nxl
  end if
  call MPI_BARRIER(MPI_COMM_WORLD,ierr)
  call MPI_Reduce(nxl,int_sum,1,&
       MPI_INTEGER,MPI_SUM,&
       0,MPI_COMM_WORLD,ierr)
  if(myid == 0) then
     if (nx .ne. int_sum) then
        write(*,*) 'Something is wrong with the number of points in x-direction: ',&
             nx,int_sum
     end if
  end if

  ! Determine the neighbours of processor px
  p_left  = px-1 - 1
  p_right = px+1 - 1
  if (px .eq. px_max) p_right = MPI_PROC_NULL
  if (px .eq. 1) p_left = MPI_PROC_NULL  

  ! Set up the x and y grids.
  allocate(x(0:(nxl+1)), y(0:(int(ny)-1)))

  !$omp parallel do private(j)
  do i = 1,nxl
     x(i) = -1.d0 + dble(i-1+ix_off)*hx
  end do
  !$omp end parallel do

  !$omp parallel do private(j)
  do j = 0, (int(ny)-1)
     ! Set up y.
     y(j) = -1 + j*hx
  end do

  ! send to left recieve from right
  call MPI_Sendrecv(x(1),1,MPI_DOUBLE_PRECISION,p_left,123,&
       x(nxl+1),1,MPI_DOUBLE_PRECISION,p_right,123,MPI_COMM_WORLD,status,ierr)
  ! send to right recieve from left
  call MPI_Sendrecv(x(nxl),1,MPI_DOUBLE_PRECISION,p_right,125,&
       x(0),1,MPI_DOUBLE_PRECISION,p_left,125,MPI_COMM_WORLD,status,ierr)
  call MPI_BARRIER(MPI_COMM_WORLD,ierr)
     
  ! Create time step intervals.
  t_final = 2
  dt = 0.5*hx
  nt = floor(t_final / dt) + 1
  dt = t_final / nt

  ! Set up initial data (i.e., obtain u0 and u1).
  ! Allocate memory.
  allocate(u0(0:(int(nx)-1), nxl+1))
  allocate(f(0:(int(nx)-1), nxl+1))
  allocate(L(0:(int(nx)-1), nxl+1))
  allocate(force(0:(int(nx)-1), nxl+1))
     
  call uExact(u0, x, y, 0.d0, nxl+1, size(y), w, kx, ky)

  !print *, "Made it past uExact."

  ! Calculate f.
  call f2(f, x, y, w, kx, ky, nxl+1, size(y))

  !print *, "Made it past f."
  
  ! Calculate Laplacian.
  call laplacian(L, u0, hx, hy, nxl+1, size(y))

  !print *, "Made it past Laplacian."

  ! Compute forcing.
  call forcing(force, 0.d0, x, y, w, kx, ky, nxl+1, size(y))

  !print *, "Made it past forcing."
  
  ! Compute u1.
  allocate(u1(0:(int(nx)-1), nxl+1))
  do j = 0, (int(nx)-1)
     do k = 1, nxl
        u1(j, k) = u0(j, k)+dt*f(j, k)+0.5*(dt**2)*(L(j, k)+force(j, k))
     end do
  end do

  ! Update BC.
  !$omp parallel do private(j)
  do j = 1, nxl
     u1(0, j) = sin(w*dt-kx*x(j))*sin(ky*y(0))
  end do
  !$omp end parallel do
  !print *, "Made it past bottom border."
  !$omp parallel do private(j)
  do j = 1, nxl
     u1((int(nx)-1), j) = sin(w*dt-kx*x(j))*sin(ky*y(int(nx)-1))
  end do
  !$omp end parallel do
  !print *, "Made it past top border."
  !$omp parallel do private(j)
  do j = 0, size(y)-1
     u1(j, 0) = sin(w*dt-kx*x(0))*sin(ky*y(j))
  end do
  !$omp end parallel do
  !print *, "Made it past left border."
  !$omp parallel do private(j)
  do j = 0, size(y)-1
     u1(j, nxl) = sin(w*dt-kx*x(int(nx)-1))*sin(ky*y(j))
  end do
  !$omp end parallel do
  !print *, "Made it past right border."

  ! Allocate u2.
  allocate(u2(0:(int(nx)-1), nxl+1))

  ! Begin subsequent computations.
  do k = 0, (int(nt)-2)

     t = (k+1)*dt

     !call MPI_Sendrecv(u0(0:(size(y)-1), 1),size(y),MPI_DOUBLE_PRECISION,p_left,125,&
     !     u0(0:(size(y)-1), nxl+1),size(y),MPI_DOUBLE_PRECISION,p_right,123,MPI_COMM_WORLD,status,ierr)
     ! send to right recieve from left
     !call MPI_Sendrecv(u0(0:(size(y)-1), nxl),size(y),MPI_DOUBLE_PRECISION,p_right,123,&
     !     u0(0:(size(y)-1), 0),size(y),MPI_DOUBLE_PRECISION,p_left,125,MPI_COMM_WORLD,status,ierr)
     !call MPI_BARRIER(MPI_COMM_WORLD,ierr)

     ! Obtain right-hand side.
     call forcing(force, t, x, y, w, kx, ky, nxl+1, size(y))
     call laplacian(L, u1, hx, hy, nxl+1, size(y))

     !print *, "Computed forcing and laplacian inside of loop."

     ! March in time.
     do j2  = 0, (int(ny)-1)
        do k2 = 1, nxl
           u2(j2, k2) = 2*u1(j2, k2)-u0(j2, k2)+(dt**2)*(L(j2, k2)+force(j2, k2))
        end do
     end do

     ! Update BC.
     !$omp parallel do private(j2)
     do j2 = 1, nxl
        u2(0, j2) = sin(w*(dt+t)-kx*x(j2))*sin(ky*y(0))
     end do
     !$omp end parallel do
     !$omp parallel do private(j2)
     do j2 = 1, nxl
        u2((int(nx)-1), j2) = sin(w*(dt+t)-kx*x(j2))*sin(ky*y(int(nx)-1))
     end do
     !$omp end parallel do
     !$omp parallel do private(j2)
     do j2 = 0, size(y)-1
        u2(j2, 0) = sin(w*(dt+t)-kx*x(0))*sin(ky*y(j2))
     end do
     !$omp end parallel do
     !$omp parallel do private(j2)
     do j2 = 0, size(y)-1
        u2(j2, nxl) = sin(w*(dt+t)-kx*x(int(nx)-1))*sin(ky*y(j2))
     end do
     !$omp end parallel do

     ! Switch solution at different time intervals.
     u0 = u1
     u1 = u2

  end do

  ! Compute the exact solution at t_final.
  allocate(uEx(0:(size(y)-1), nxl+1))
  allocate(uError(0:(size(y)-1), nxl+1))
  call uExact(uEx, x, y, t_final, nxl+1, size(y), w, kx, ky)
  maxVal = 0
  !$omp parallel do private(j, k)
  do j = 0, (size(y)-1)
     do k = 1, nxl
        uError(j, k) = abs(uEx(j, k)-u2(j, k))
     end do
  end do
  !$omp end parallel do

  !$omp parallel do private(j, k)
  do j = 0, (size(y)-1)
     do k = 1, nxl
        if (uError(j, k) > maxVal) then
           maxVal = uError(j, k)
        end if
     end do
  end do
  !$omp end parallel do

  ! Store error in E.
  err = maxVal
     
  ! Deallocate all arrays.
  !deallocate(x, y, u0, f, L, force, u1, u2, uEx, uError)

  ! Print out value.
  print *, 'h:', hx, 'error:', err

  !print *, "Process: ", px
  !do i = 0, size(y)-1
     !do j = 1, nxl
        !print *, "uError(", i, ",", j, "): = ", uError(i, j)
     !end do
  !end do
  call MPI_BARRIER(MPI_COMM_WORLD,ierr)
  mpi_t1=mpi_wtime()
  mpi_dt=mpi_t1-mpi_t0
  if (myid .eq. 0) then
     print *, nprocs, "processes,", mpi_dt, "wall clock time."
  end if

  

  call mpi_finalize(ierr)

end program wave2Dserial

! Subroutine which calcultes the exact value of u.
! IMPORTANT: This subroutine is written under the assumption that
!            u is square dimension. Do not attempt to use a
!            array with differing dimensions. 
subroutine uExact(u, x, y, t, xsize, ysize, w, kx, ky)
  implicit none

  ! Declaration of variables.
  integer, intent(in) :: ysize, xsize ! Size of all arrays.
  real(kind = 8), intent(out) :: u(ysize, xsize) ! Final output.
  real(kind = 8) :: x(xsize), y(xsize), t, w, kx, ky
  integer :: i, j ! Iterators. 

  ! Perform calculation.
  !$omp parallel do private(i, j)
  do i = 1, ysize
     do j = 2, xsize-1
        u(i, j) = sin(w*t-kx*x(j))*sin(ky*y(i))
     end do
  end do
  !$omp end parallel do
  
end subroutine uExact

! Subroutine which will compute f on our wave at each grid point.
! WARNING: Just as with uExact, this subroutine will run off of the assumption
! of nx = ny.
subroutine f2(fa, x, y, w, kx, ky, nx, ny)

  implicit none

  ! Declaration of variables & arrays.
  integer, intent(in) :: nx,ny
  real(kind = 8), intent(out) :: fa(ny, nx) ! Final output.
  real(kind = 8) :: x(nx), y(ny), w, kx, ky
  integer :: i, j ! Iterators.

  ! Perform calculation
  !$omp parallel do private(i, j)
  do i = 1, ny
     do j = 2, nx-1
       fa(i, j) = w*cos(kx*x(j))*sin(ky*y(i))
     end do
  end do
  !$omp end parallel do
  
end subroutine f2

! Subroutine which will compute the Laplacian.
! WARNING: Assumption of nx = ny.
subroutine laplacian(L, u, hx, hy, nx,ny)

  implicit none

  ! Declaration of variables & arrays.
  integer, intent(in) :: nx,ny
  real(kind = 8), intent(in) :: u(nx, ny)
  real(kind = 8), intent(out) :: L(nx, ny) ! Final output.
  real(kind = 8) :: hx, hy
  integer :: i, j ! Iterators.

  ! Perform computation.
  !$omp parallel do private(i, j)
  do i = 2, (ny-1)
     do j = 3, (nx-2)
        L(i, j)=(1/hx**2)*u(i,j+1)+(1/hx**2)*u(i,j-1)+(1/hy**2)*u(i+1,j)+(1/hy**2)*u(i-1,j)-((2/hx**2)+(2/hx**2))*u(i,j)
     end do
  end do
  !$omp end parallel do
  
end subroutine laplacian

! Subroutine which will compute the forcing for the entire grid.
! WARNING: Assumption of nx = ny.
subroutine forcing(force, t, x, y, w, kx, ky, nx,ny)

  implicit none

  ! Declaration of variables.
  integer, intent(in) :: nx,ny
  real(kind = 8), intent(in) :: w, kx, ky
  real(kind = 8), intent(in) :: x(nx), y(ny)
  real(kind = 8) :: t
  real(kind = 8), intent(out) :: force(nx, ny)
  integer :: i, j

  ! Perform computation.
  !$omp parallel do private(i, j)
  do i = 1, ny
     do j = 2, nx-1
        force(i, j) = -(w**2-kx**2-ky**2)*sin(w*t-kx*x(j))*sin(ky*y(i))
     end do
  end do
  !$omp end parallel do
  
end subroutine forcing

! Example.
subroutine f(n, r)

  implicit none
 
  integer, intent(in) :: n
  real(kind = 8), intent(out) :: r(n)
  integer :: i
  do i = 1, n
     r(i) = r(i) * 2
  end do

end subroutine f



