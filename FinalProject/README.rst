++++++++++++++++++++++++++++++++++++++++++++
Parallel Computing II: Wave Equation Via MPI
++++++++++++++++++++++++++++++++++++++++++++

- All of the files used in our final project will be located in this repository.

  - Code ran on Stampede2 to obtain the correct timings for different grid sizes & number of processors.
  - Strong and weak scaling analysis.

    - In order to replicate results, open up the directory named Code and compile the file wave2D_serialMPI.f90.
  - NOTE: Use the MPI compiler when doing so.
  - This will observe the error given in parallel as well as return the wall clock time for computation.
  - These results were used in creating the results displayed in our final report (.pdf file).
  - The final report (.pdf) can be found in the root directory and is named parallel-computing-ii.pdf.
  - Serial results can also be found from the file wave2D_serial.f90.
  - These results are supported by theory and converge correctly.

    - COMPLETION NOTICE: Due to one of the people working on these departing the United States before the due date, our results for the parallelized code via MPI did not converge in an appropriate manner. The convergence failed as we were implementing MPI to communicate between processors at compile time.
 
