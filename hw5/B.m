function dx = B(t,x)
  global gamma1  gamma2;
  dx = [0;0];
  gamma1 = 1;
  gamma2 = 1;
  dx(1) = gamma1*x(1);
  dx(2) = gamma2*(x(1)-x(2));
  
  [t,x] = ode45('B',[0,100],[10,10]);
  plot(t,x);
  
  