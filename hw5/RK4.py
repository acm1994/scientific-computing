from random import *
import matplotlib.pyplot as plt
import math
from matplotlib import animation

####################################################
# FUNCTION DECLARATIONS ############################
####################################################

# Declaration of empty lists (i.e., arrays.)
xFeed = 0 # X-coordinates (feeder).
yFeed = 0 # Y-coordinates (feeder).
x = [] # X-coordinates. (bird)
y = [] # Y-coordinates. (bird)
initForceX = [] # Initial force. (x)
flockForceX = [] # Flocking force. (x)
repForceX = [] # Repelling force. (x)
initForceY = [] # Initial force. (y)
flockForceY = [] # Flocking force. (y)
repForceY = [] # Repelling force. (y)

predForceX = [] # Predator force (X).
predForceY = [] # Predator force (y).

# Declaration of constants.
gamma1 = 3.2
gamma2 = 2.9
gamma3 = 1.7 # Predator constant.
kappa = 0.5
delta = 0.24
h = 0.01
ro = 0.2
ro2 = 0.3 # Stinky constant.
hFx = 0
hFy = 0

# Vectors which will hold B' (for x and y coordinates).
BprimeX = []
BprimeY = []

# Vectors which will hold B (for x and y coordinates).
bX = []
bY = []

# Function which will declare thesize of all arrays (i.e., number of birds).
def numBirds(N):
    for i in range(0, N):
        # Create vectors of size N.
        x.append(0)
        y.append(0)
        initForceX.append(0)
        flockForceX.append(0)
        repForceX.append(0)
        initForceY.append(0)
        flockForceY.append(0)
        repForceY.append(0)
        BprimeX.append(0)
        BprimeY.append(0)
        bX.append(0)
        bY.append(0)
        predForceX.append(0)
        predForceY.append(0)

# Function which will initialize the locations at t = 0.
def initLocation(N):   
    for i in range(0, N):
        x[i] = uniform(0, 3)*(math.cos(math.radians(uniform(0, 365))))
        #y[i] = uniform(-2, 2)
        y[i] = uniform(0, 3)*(math.sin(math.radians(uniform(0, 365))))
    
# X-Coordinate of bird feeder at time t.
# Param: Time-step t.
def xFeeder(t):
    # X position.
    return 3*math.cos(math.radians(t))

# Y-Coordinate of bird feeder at time t.
# Param: Time-step t.
def yFeeder(t): 
    # Y position.
    return 3*math.sin(math.radians(t))
        
# Functions which will calculate the center of mass (for
# both the x and y coordinates).
def centMassX(X):
    
    # The parameters of this function will be lists containing
    # the X coordinates of each bird, respectively.
    # Therefore, we will sum up all of the values and divide
    # them by the total number of birds N.
    summ = 0
    for coord in X:
        summ = summ + coord
        
    # Now that we have the sum of coordinates, we need to
    # divide by the number of birds N.
    return summ / (len(X))
        
# Y coordinates for center of mass.
def centMassY(Y):
    
    # Same process as centMassX.
    summ = 0
    for coord in Y:
        summ = summ + coord
        
    return summ / (len(Y))

# Determine initial force in x-direction (F1). 
# Param: X coordinates of feeder, X coordinates of birds.
def initFx(birdX, feederX): 
    
    # Since the first bird (leader) is going to behave
    # differently from the rest of the flock, we will first
    # do a special computation on him. 
    initForceX[0] = gamma1*(feederX - birdX[0])
    
    # Go through each entry in each array (under
    # the assumption that both are of equal length).
    for i in range(1, len(birdX)):
        initForceX[i] = gamma2*(birdX[0] - birdX[i])
        
    return initForceX
        
# Determine initial force in y-direction. (F1)
# Param: Y-coordinates of feeder, Y-coordinates of birds.
def initFy(birdY, feederY):
    
    # Same process as in initFx.
    initForceY[0] = gamma1*(feederY - birdY[0])
    for i in range(1, len(birdY)):
        initForceY[i] = gamma2*(birdY[0] - birdY[i])
        
    return initForceY
        
# Determine flocking force in the x-direction. (F2)
# Param: X-coordinates of all birds. 
def flockX(X):
    
    # For this function, we are going to implement the centMassX 
    # function to calculate the center of mass (x-coordinate).
    xMass = centMassX(X)
    
    # The first bird will have a value of 0 for this part.
    flockForceX[0] = 0
    
    # Subtract x-coordinate of bird k from the center of mass X.
    for i in range(1, len(X)):
        flockForceX[i] = kappa*(xMass - X[i])
        
    return flockForceX
        
# Determine flocking force in the y-direction. (F2)
# Param: Y-coordinates of all birds.
# Same process as in flockX.
def flockY(Y):
    yMass = centMassY(Y)
    flockForceY[0] = 0
    for i in range(1, len(Y)):
        flockForceY[i] = kappa*(yMass - Y[i])
        
    return flockForceY

    
# Determine repelling force in the x-direction for a single bird.
# Param: k - bird we are interested in. 
def repelForceX(X, k):
    
    # Refer to global delta constant and middle bird.
    global delta 
    
    # Initialize sum variable.
    summ = 0

    for i in range(1, len(x)):
        if k == 2:
            summ = summ + ro2*((X[k-1] - X[i]) / ((X[k-1] - X[i])**2 + delta))
        else:
            summ = summ + ro*((X[k-1] - X[i]) / ((X[k-1] - X[i])**2 + delta))
        
    
    return summ
    
# Determine repelling force in the y-direction for a single bird.
# Param: k - bird we are interested in.
def repelForceY(Y, k):
    
    # Refer to global delta constant and middle bird.
    global delta
    
    # Initialize sum variable.
    summ = 0
        
    for i in range(1, len(x)):
        if k == 2:
            summ = summ + ro2*((Y[k-1] - Y[i]) / ((Y[k-1] - Y[i])**2 + delta))
        else:
            summ = summ + ro*((Y[k-1] - Y[i]) / ((Y[k-1] - Y[i])**2 + delta))

    return summ
        
# Return all of the repelling forces in the x-direction. Return vector.
# Param: None
def repellingForcesX(X):
    
    # Set repelling force on the first bird to 0.
    repForceX[0] = 0
    
    for i in range(2, len(x)+1):
        repForceX[i-1] = repelForceX(X, i)
        
    return repForceX
    
# Return all of the repelling forces in the y-direction. Return vector.
# Param: None
def repellingForcesY(Y):
    
    # Set repelling forceo n the first bird to 0.
    repForceY[0] = 0
    
    for i in range(2, len(y)+1):
        repForceY[i-1] = repelForceY(Y, i)
        
    return repForceY
    
# RK1 (x-coordinates)
# Param: i - frame number (to be used in animation)
def RK1x(i):
    
    # Refer to global x and y coordiantes.
    global x
    
    # Initialize feeder position.
    X = [xFeeder(i)]
    k1 = list(map(lambda x, k, m: x + k + m, initFx(x, X[0]), flockX(x), repellingForcesX(x)))

    # Return value.
    return k1

# RK1
# Param: i - frame number
# RK1 (y-coordinates)
# Param: i - frame number (to be used in animation).
def RK1y(i):
    
    # Refer to global y-coordinates.
    global y
    
    # Initialize feeder position (y-coordinates).
    Y = [yFeeder(i)]
    l1 = list(map(lambda x, k, m: x + k + m, initFy(y, Y[0]), flockY(y), repellingForcesY(y)))
    
    # Return value.
    return l1

# RK2 (x-coordinates)
# Param: i - frame number.
#        rk1 - Runge-kutta 1 list.
def RK2x(i, rk1):
    
    # Refer to global variables.
    global x 
    global h
    
    # Initialize feeder position (x-coordinates).
    X = [xFeeder(i+h/2)]
    
    # Temporary x variable.
    xTemp = list(map(lambda g, r: g + (h*r)/2, x, rk1))
    
    rk2 = list(map(lambda x, k, m: x + k + m, initFx(xTemp, X[0]), flockX(xTemp), repellingForcesX(x)))
    
    # Return value.
    return rk2
    
# RK2 (y-coordinates)
# Param: i - frame number. 
#       
def RK2y(i, lk1):
    
    # Refer to global variables.
    global y 
    global h
    
    # Initialize feeder position (x-coordinates).
    Y = [yFeeder(i+h/2)]
    
    # Temporary x variable.
    yTemp = list(map(lambda g, r: g + (h*r)/2, y, lk1))
    
    lk2 = list(map(lambda x, k, m: x + k + m, initFy(yTemp, Y[0]), flockY(yTemp), repellingForcesY(y)))
    
    # Return value.
    return lk2
    
# RK3 (x-coordinates)
# Param: i - frame 
def RK3x(i, rk2):
    
    # Refer to global variables.
    global x 
    global h
    
    # Initialize feeder position (x-coordinates).
    X = [xFeeder(i+h/2)]
    
    # Temporary x variable.
    xTemp = list(map(lambda g, r: g + (h*r)/2, x, rk2))
    
    rk3 = list(map(lambda x, k, m: x + k + m, initFx(xTemp, X[0]), flockX(xTemp), repellingForcesX(x)))
    
    # Return value.
    return rk3
    
# RK3 (y-coordinates)
# Param: i - frame number. 
#       
def RK3y(i, lk2):
    
    # Refer to global variables.
    global y 
    global h
    
    # Initialize feeder position (x-coordinates).
    Y = [yFeeder(i+h/2)]
    
    # Temporary x variable.
    yTemp = list(map(lambda g, r: g + (h*r)/2, y, lk2))
    
    lk3 = list(map(lambda x, k, m: x + k + m, initFy(yTemp, Y[0]), flockY(yTemp), repellingForcesY(y)))
    
    # Return value.
    return lk3
    
# RK4 (x-coordinates)
# Param: i - frame 
def RK4x(i, rk3):
    
    # Refer to global variables.
    global x 
    global h
    
    # Initialize feeder position (x-coordinates).
    X = [xFeeder(i+h/2)]
    
    # Temporary x variable.
    xTemp = list(map(lambda g, r: g + (h*r), x, rk3))
    
    rk4 = list(map(lambda x, k, m: x + k + m, initFx(xTemp, X[0]), flockX(xTemp), repellingForcesX(x)))
    
    # Return value.
    return rk4
    
# RK3 (x-coordinates)
# Param: i - frame 
def RK4y(i, lk3):
    
    # Refer to global variables.
    global x 
    global h
    
    # Initialize feeder position (x-coordinates).
    Y = [yFeeder(i+h/2)]
    
    # Temporary x variable.
    yTemp = list(map(lambda g, r: g + (h*r), y, lk3))
    
    lk4 = list(map(lambda x, k, m: x + k + m, initFy(yTemp, Y[0]), flockY(yTemp), repellingForcesY(y)))
    
    # Return value.
    return lk4
    
# Position of predator (x-coordinates)
def predPositionX():
    
    return -3

# Position of predator (y-coordinates)
def predPositionY():
    
    return 0

# Predator force (x).
def predatorForceX(birdX):
    
    global predForceX
    
    predForceX[0] = gamma3*(birdX[0] - predPositionX())
        
    # Return value.
    return predForceX
    
# Predator force (y).
def predatorForceY(birdY):
    
    global predForceY
    
    predForceY[0] = gamma3*(birdY[0] - predPositionY())
    
    # Return value
    return predForceY
        

####################################################
# INITIALIZATION ###################################
####################################################

# Set up starting position of birds and feeder.
N = 20
numBirds(N)
initLocation(N)
massX = centMassX(x) # Center of mass. 
massY = centMassY(y)
# Initial Position of bird feeder.
xFeed = xFeeder(0)
yFeed = yFeeder(0)

## STINKY
x[1] = massX
y[1] = massY

midBird = int(len(x) / 2)

# First set up the figure, the axis, and the plot element we want to animate
fig = plt.figure()
ax = plt.axes(xlim=(-7, 7), ylim=(-7, 7))
feeder, = ax.plot([], [], 'ko', ms = 6)
birds, = ax.plot([], [], 'bo', ms = 6)
leader, = ax.plot([], [], 'ro', ms = 6)
stinky, = ax.plot([], [], 'mo', ms = 6)

####################################################
# MOVIE ############################################
####################################################

# initialization function: plot the background of each frame
def init():
    feeder.set_data([], [])
    birds.set_data([], [])
    leader.set_data([], []) 
    stinky.set_data([], [])
    return feeder, birds, leader, stinky

# animation function.  This is called sequentially
def animate(i):
    global x
    global y
    #global h
    #BprimeX = list(map(lambda x, k, m: x + k + m, initFx(x, X[0]), flockX(x), repellingForcesX(x)))
    #BprimeY = list(map(lambda x, k, m: x + k + m, initFy(y, Y[0]), flockY(y), repellingForcesY(y)))
    #hFx = list(map(lambda k: h*k, BprimeX))
    #hFy = list(map(lambda k: h*k ,BprimeY))
    #x = list(map(lambda g, h: g + h, x, hFx))
    #y = list(map(lambda g, h: g + h, y, hFy))
    #feeder.set_data(X, Y)
    #birds.set_data(x, y)
    #leader.set_data(x[0], y[0])
    #return feeder, birds, leader
    X = [xFeeder(i)] 
    Y = [yFeeder(i)]
    k1x = RK1x(i)
    k1y = RK1y(i)
    k2x = RK2x(i, k1x)
    k2y = RK2y(i, k1y)
    k3x = RK3x(i, k2x)
    k3y = RK3y(i, k2y)
    k4x = RK4x(i, k3x)
    k4y = RK4y(i, k3y)
    k = list(map(lambda a, b, c, d: (a + 2*b + 2*c + d)/6, k1x, k2x, k3x, k4x))
    l = list(map(lambda a, b, c, d: (a + 2*b + 2*c + d) / 6, k1y, k2y, k3y, k4y))
    x = list(map(lambda a, b: a + h*b, x, k))
    y = list(map(lambda a, b: a + h*b, y, l))
    birds.set_data(x, y)
    leader.set_data(x[0], y[0])
    feeder.set_data(X, Y)
    stinky.set_data(x[1], y[1])
    
    return birds, leader, feeder, stinky

# call the animator.  blit=True means only re-draw the parts that have changed.
anim = animation.FuncAnimation(fig, animate, init_func=init,
                               frames=360, interval=20, blit=True)

anim.save('stinky.mp4', fps=30, extra_args=['-vcodec', 'libx264'])