function rk4
h = 0.2;
t = 0;
w = 0.5;
fprintf('Step 0: t = %12.8f, w = %12.8f\n', t, w);
for i=1:10
k1 = h*f(t,w);
k2 = h*f(t+h/2, w+k1/2);
k3 = h*f(t+h/2, w+k2/2);
k4 = h*f(t+h, w+k3);
w = w + (k1+2*k2+2*k3+k4)/6;
t = t + h;
fprintf('Step %d: t = %6.4f, w = %18.15f\n', i, t, w);
end
plot(t,w);
%test function%
%%%%%%%%%%%%%%%%%%
function v = f(t,y)
v = y-t^2+1;