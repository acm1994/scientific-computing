% Joseph Murrietta and Aaron Montano
% Angry Birds: ODE Computation
% Responsible for all of the computation for
% solving the ODE.

% Number of birds.
n = 10;

% Gamma and Kappa values.
gamma1 = 3;
gamma2 = 2;
kappa = 2.5;

  
  
