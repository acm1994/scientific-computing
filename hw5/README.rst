+++++++++++++++++++++++++++++++++++++++++
Angry Birds: ODE Modeling and Computation
+++++++++++++++++++++++++++++++++++++++++

- In this homework, we are going to be exploring the movement of a bird flock through the use of a system of ordinary differential equations.
- In order to see our results, you may run either RK4.py or rk4.m.
- This repository contains three movies: one contains a clip which simply shows the implementation of the Runge-Kutta 4 algorithm, another shows the same algorithm under the influence of a stationary predatory force (as marked by the green star to the center left of the figure) and the other one shows the same flock of birds with the smelly bird, as marked by the purple dot. 
- All movies are .mp4 files. The final report which presents our findings is the file named angry-birds-ode-3.pdf.
