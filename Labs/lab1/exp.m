x = linspace(-1,1,10);
y = [x; exp(1/2-sin(5*pi*x))];
fid = fopen('exp.txt','w');
fprintf(fid,'%6.2f & %12.8f \\\\ \n',y);
fclose(fid);
