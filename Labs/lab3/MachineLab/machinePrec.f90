! Joseph Murrietta and Aaron Montano

subroutine machineLab

  implicit none

  double precision :: i, h, final

  h = 1
  i = 10d-12
  final = 0

  do while (i < h)

     final = abs(1 - (exp(i) - 1) / (i))

     i = i + i*2

     print *, final
     
  end do

  

end subroutine machineLab

program machinePrec

  call machineLab

end program machinePrec


