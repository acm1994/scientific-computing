! Joseph Murrietta and Aaron Montano
! 09/26/2017
! Matrix Multiplication Algorithm and Computation Time


! In this simple program, we are going to be performing simple
! matrix multiplication. In addition, we are also going to check
! to see whether or not the computation time of two n x n matrices
! being multiplied together is consistent with O(n^3).
program cpuTest

  implicit none

  ! Declaration of variables.
  integer :: n, i, j, k ! Size of matrix and loop iterators, respectively.\
  
  ! Declaration of sum (to be used in algorithm).
  integer :: sum

  ! Declaration of variables used to time the functions.
  real :: t1, t2

  ! Declare all matrices for each n-value.
  INTEGER, DIMENSION(100, 100) :: A_1, B_1, C_1
  INTEGER, DIMENSION(200, 200) :: A_2, B_2, C_2
  INTEGER, DIMENSION(400, 400) :: A_3, B_3, C_3
  INTEGER, DIMENSION(800, 800) :: A_4, B_4, C_4
 
  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  !! n = 100 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  n = 100
  
  ! Fill out arrays.
  do i = 1, n

     do j = 1, n

        A_1(i, j) = 1
        B_1(i, j) = 2

     end do

  end do

  ! Matrix multiplication algorithm.
  call cpu_time (t1)
  do i = 1, n
     
     do j = 1, n
        
        sum = 0
        
        do k = 1, n
           
           sum = sum + A_1(i, k) * B_1(k, j)
           
        end do

        C_1(i, j) = sum

     end do
     
  end do
  call cpu_time (t2)
  write (*, *) "Elapsed computation time (n = 100): ", (t2-t1)

  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  !! n = 200 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  n = 200

  ! Fill out arrays.
  do i = 1, n

     do j = 1, n

        A_2(i, j) = 1
        B_2(i, j) = 2

     end do

  end do

  call cpu_time (t1)
  ! Matrix multiplication algorithm.
  do i = 1, n

     do j = 1, n

        sum = 0

        do k = 1, n

           sum = sum + A_2(i, k) * B_2(k, j)

        end do

        C_2(i, j) = sum

     end do

  end do
  call cpu_time (t2)
  write (*, *) "Elapsed computation time (n = 200): ", (t2-t1)

  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  !! n = 400 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  n = 400
  
  ! Fill in matrices.
  do i = 1, n

     do j = 1, n

        A_3(i, j) = 1
        B_3(i, j) = 2

     end do

  end do

  ! Matrix multiplication algorithm.
  call cpu_time (t1)
  do i = 1, n

     do j = 1, n

        sum = 0

        do k = 1, n

           sum = sum + A_3(i, k) * B_3(k, j)

        end do

        C_3(i, j) = sum

     end do

  end do
  call cpu_time (t2)
  write(*, *) "Elapsed computation time (n = 400): ", (t2 - t1)

  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  !! n = 800 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  n = 800
  
  ! Fill out matrices.
  do i = 1, n

     do j = 1, n

        A_4(i, j) = 1
        B_4(i, j) = 2

     end do

  end do

  ! Matrix multiplication algorithm.
  call cpu_time (t1)
  do i = 1, n

     do j = 1, n

        sum = 0

        do k = 1, n

           sum = sum + A_4(i, k) * B_4(k, j)

        end do

        C_4(i, j) = sum

     end do

  end do
  call cpu_time (t2)
  write(*, *) "Elapsed computation time (n = 800): ", (t2 - t1)
  
end program cpuTest
